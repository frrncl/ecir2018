%% import_collection_crowd
%
% Imports the requested collection and saves it to a |.mat| file.
%
%% Synopsis
%
%   [] = import_collection_crowd(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
%
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = import_collection_crowd(trackID)
    
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % set up the common parameters
    common_parameters

    % disable warnings, not needed for bulk import
    warning('off');

    % start of overall import
    startImport = tic;

    fprintf('\n\n######## Importing collection %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - imported on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
   

    % import the gold standard pool
    start = tic;

    fprintf('+ Importing the gold standard pool\n');
    
    % creating local input parameters for import
    fileName = EXPERIMENT.track.(trackID).file.goldPool;
    goldPoolID = EXPERIMENT.pattern.identifier.goldPool(trackID);
        
    % importing the pool
    evalf(EXPERIMENT.track.(trackID).importGoldPool, {'fileName', 'goldPoolID'}, {goldPoolID});

    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    % the total number of topics in the collection
    evalf(@height, {goldPoolID}, {'T'})
    fprintf('  - total number of topics: %d\n\n', T);

    % the list of required topics
    evalf(@(x) x.Properties.RowNames, {goldPoolID}, {'requiredTopics'})
    
    % import the participant pools
    start = tic;

    fprintf('+ Importing the participant pools\n');

    % return the list of text files in the directory. It ignores non-text
    % files.
    files = dir(sprintf('%1$s%2$s', EXPERIMENT.track.(trackID).path.crowd, '*.txt'));

    % the total number of pools/assessors
    P = length(files);

    % extract an horizontal cell array of file names
    [poolFiles{1:P}] = deal(files.name);

    % the labels used to prefix the pool identifiers (same as file name but
    % without extension)
    poolLabels = cellfun(@(s) {s(1:end-4)}, poolFiles, 'UniformOutput', true);

    % the identifiers of the imported pools
    poolIdentifiers = cellfun(@(s) {EXPERIMENT.pattern.identifier.poolCrowd( s, trackID)}, poolLabels, 'UniformOutput', true);

    % concatenate the file names with the directory
    poolFiles = cellfun(@(s) {sprintf('%1$s%2$s', EXPERIMENT.track.(trackID).path.crowd, s)}, poolFiles, 'UniformOutput', true);

    % import the pools from participants
    for p = 1:P

        % creating local input parameters for import
        fileName = poolFiles{p};
        goldPoolID =  poolIdentifiers{p};
        
        evalf(EXPERIMENT.track.(trackID).importCrowdPool, {'fileName','goldPoolID', 'requiredTopics'}, {goldPoolID});
        
    end;

    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
    fprintf('  - total number of pools: %d\n\n', P);


    % import the runs in trec_eval document ordering
    fprintf('+ Importing the run set according to the standard trec_eval document ordering\n');

    runSetIdentifiers = cell(1, EXPERIMENT.track.(trackID).runSet.originalTrackID.number);

    
    % import each run set
    for r = 1:EXPERIMENT.track.(trackID).runSet.originalTrackID.number

        start = tic;

        fprintf('  - original track of the runs: %s\n', EXPERIMENT.track.(trackID).runSet.originalTrackID.id{r});
        
        runSetIdentifiers{r} = EXPERIMENT.pattern.identifier.runSet(trackID, EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

        % creating local input parameters for import
        fileName = EXPERIMENT.track.(trackID).path.runSet(EXPERIMENT.track.(trackID).runSet.originalTrackID.id{r});
        goldPoolID =  runSetIdentifiers{r};
        
        evalf(EXPERIMENT.track.(trackID).importRunSet, {'fileName', 'goldPoolID', 'requiredTopics'}, {goldPoolID});
          
        fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

        evalf(@width, {goldPoolID}, {'R'})    
        fprintf('    * total number of runs: %d\n', R);
    end;
    fprintf('\n');

    start = tic;

    fprintf('+ Saving the data set\n');
    
    sersave (EXPERIMENT.pattern.file.crowdDataset(trackID, trackID), ...
            'poolLabels', 'poolIdentifiers', 'T', 'P', ...
            runSetIdentifiers{:}, ...
            EXPERIMENT.pattern.identifier.goldPool(trackID), ...
            poolIdentifiers{:});

    fprintf('  - elapsed time: %s\n\n', elapsedToHourMinutesSeconds(toc(start)));


    fprintf('\n\n######## Total elapsed time for importing collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startImport)));

    % stop logging
    diary off;

    % re-enable warnings
    warning('on');

end


