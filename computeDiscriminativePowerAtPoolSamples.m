%% computeDiscriminativePowerAtPoolSamples
% 
% Load the measures computed among the downsampled pools. For each iteration and pool sample, 
% it computes the discriminative power for every measure and saves it to a |.mat| file.
%
%% Synopsis
%
%   [asl, dp, delta, replicates] = computeDiscriminativePowerAtPoolSamples(varargin)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|pListID |* - the identifier of the probability list to process, if any.
%
% *Returns*
%
% Nothing
%
%
%% Variables to be saved
%
% * *|asl|*  - is a table containing a row for each iteration and a 
% column for each pool sample. The value of each cell is the ASL
% computed on the given pool sample at the given iteration.
% * *|dp|*  - is a table containing a row for each iteration and a 
% column for each pool sample. The value of each cell is the discriminative
% power computed on the given pool sample at the given iteration.
% * *|delta|*  - is a table containing a row for each iteration and a 
% column for each pool sample. The value of each cell is the delta
% computed on the given pool sample at the given iteration.
% * |replicates| - is an integer-valued matrix containing the
% replicates that have been used for the bootstrap or randomization. In the case of
% the |PairedBootstrapTest|, it is a |t*samples| matrix (|t| number of
% topics) where each column (a replicate) represents a sampling with 
% repetitions of the topics. In the case of the |RandomisedTukeyHSDTest|,
% it is a |samples*(t*s)| matrix (|t| number of topics, |s| number of 
% systems) where each row (a replicate) represents a topic-by-topic random
% permutation of a full |measuredRunSet|; each row contains the linear
% indexes needed to produce such permutation.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% <mailto:silvello@dei.unipd.it Gianmaria Silvello>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2013-2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}


%%
function [] = computeDiscriminativePowerAtPoolSamples(trackID, pListID)


    iterations = 1;

    % check that we have the correct number of input arguments.
    narginchk(2, Inf);

    % setup common parameters
    common_parameters;

    % parse the variable inputs
    pnames = {'Replicates'};
    dflts =  {[]};
    
    if verLessThan('matlab', '9.2.0')
        [replicates, supplied, otherArgs] = matlab.internal.table.parseArgs(pnames, dflts);
    else
        [replicates, supplied, otherArgs] = matlab.internal.datatypes.parseArgs(pnames, dflts);
    end
    
    


    if supplied.Replicates
        % check that replicates is a nonempty integer-valued matrix
        validateattributes(replicates, {'numeric'}, ...
            {'nonempty', 'integer', '>', 0}, '', 'Replicates');
    end;

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    % check that pListID is a non-empty string
    validateattributes(pListID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'pListID');

    if iscell(pListID)
        % check that pListID is a cell array of strings with one element
        assert(iscellstr(pListID) && numel(pListID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected pListID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    pListID = char(strtrim(pListID));
    pListID = pListID(:).';

    % check that pListID assumes a valid value
    validatestring(pListID, ...
        fieldnames(EXPERIMENT.measure.gap.probability), '', 'pListID');

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing discriminative power  on collection %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


    measureIDList = [];
    measureLabel = [];

    for m = 1: EXPERIMENT.measure.number

        mid = EXPERIMENT.measure.list{m};

        if isfield(EXPERIMENT.measure.(mid),'probability')

            pList = EXPERIMENT.measure.(mid).probability.(pListID);

            for p = 1 : length(pList)

                pid = pList{p};

                measureIDList = [measureIDList {EXPERIMENT.pattern.identifier.downsampledMeasure(EXPERIMENT.pattern.identifier.pid(mid, pid), trackID)}];

                measureLabel = [measureLabel {EXPERIMENT.pattern.identifier.pid(EXPERIMENT.measure.getAcronym(m), pid)}];

            end

        else
            measureIDList = [measureIDList {EXPERIMENT.pattern.identifier.downsampledMeasure(mid, trackID)}];

            measureLabel = [measureLabel {EXPERIMENT.measure.getAcronym(m)}];

        end;
    end

    measureNumber = length(measureIDList);


    fprintf('+ Loading measures\n');

    measures = [];

    % for each measure
    for m = 1:measureNumber


        fprintf('+ loading %s\n', measureIDList{m});

        % load the measure in the tmp variable
        serload2(EXPERIMENT.pattern.file.measure(trackID, measureIDList{m}), ...
            'WorkspaceVarNames', {'measuredRunSet'}, ...
            'FileVarNames', {measureIDList{m}});

        if  iterations ~= measuredRunSet.Properties.UserData.iterations
            error('MATTERS:IllegalArgument', 'The function works for only %d iterations of the downsampling of the pool, while the measure %s contains %d iterations', iterations, measureIDList{m}, measuredRunSet.Properties.UserData.iterations);
        end;

        % check that measured run set is a non-empty table
        validateattributes(measuredRunSet, {'table'}, {'nonempty'}, '', 'measuredRunSet', 1);

        sampleNum = length(measuredRunSet.Properties.UserData.sampleSize) + 1;

        % the ASL at different iterations and pool samples
        asl = cell2table(cell(iterations, sampleNum));
        asl.Properties.UserData.identifier = measuredRunSet.Properties.UserData.identifier;
        asl.Properties.UserData.pool = measuredRunSet.Properties.UserData.pool;
        asl.Properties.UserData.funcName = measuredRunSet.Properties.UserData.funcName;
        asl.Properties.UserData.downsampling = measuredRunSet.Properties.UserData.downsampling;
        asl.Properties.UserData.shortDownsampling = measuredRunSet.Properties.UserData.shortDownsampling;
        asl.Properties.UserData.name = measuredRunSet.Properties.UserData.name;
        asl.Properties.UserData.shortName = measuredRunSet.Properties.UserData.shortName;
        asl.Properties.UserData.sampleSize = measuredRunSet.Properties.UserData.sampleSize;
        asl.Properties.UserData.iterations = measuredRunSet.Properties.UserData.iterations;
        %         asl.Properties.UserData.ciAlpha = ciAlpha;
        asl.Properties.RowNames = measuredRunSet.Properties.RowNames;
        asl.Properties.VariableNames= measuredRunSet{1, 1}{1, 1}.Properties.VariableNames;

        dp = array2table(NaN(iterations, sampleNum));
        dp.Properties.UserData.identifier = measuredRunSet.Properties.UserData.identifier;
        dp.Properties.UserData.pool = measuredRunSet.Properties.UserData.pool;
        dp.Properties.UserData.funcName = measuredRunSet.Properties.UserData.funcName;
        dp.Properties.UserData.downsampling = measuredRunSet.Properties.UserData.downsampling;
        dp.Properties.UserData.shortDownsampling = measuredRunSet.Properties.UserData.shortDownsampling;
        dp.Properties.UserData.name = measuredRunSet.Properties.UserData.name;
        dp.Properties.UserData.shortName = measuredRunSet.Properties.UserData.shortName;
        dp.Properties.UserData.sampleSize = measuredRunSet.Properties.UserData.sampleSize;
        dp.Properties.UserData.iterations = measuredRunSet.Properties.UserData.iterations;
        %         dp.Properties.UserData.ciAlpha = ciAlpha;
        dp.Properties.RowNames = measuredRunSet.Properties.RowNames;
        dp.Properties.VariableNames= measuredRunSet{1, 1}{1, 1}.Properties.VariableNames;

        delta = array2table(NaN(iterations, sampleNum));
        delta.Properties.UserData.identifier = measuredRunSet.Properties.UserData.identifier;
        delta.Properties.UserData.pool = measuredRunSet.Properties.UserData.pool;
        delta.Properties.UserData.funcName = measuredRunSet.Properties.UserData.funcName;
        delta.Properties.UserData.downsampling = measuredRunSet.Properties.UserData.downsampling;
        delta.Properties.UserData.shortDownsampling = measuredRunSet.Properties.UserData.shortDownsampling;
        delta.Properties.UserData.name = measuredRunSet.Properties.UserData.name;
        delta.Properties.UserData.shortName = measuredRunSet.Properties.UserData.shortName;
        delta.Properties.UserData.sampleSize = measuredRunSet.Properties.UserData.sampleSize;
        delta.Properties.UserData.iterations = measuredRunSet.Properties.UserData.iterations;
        %         delta.Properties.UserData.ciAlpha = ciAlpha;
        delta.Properties.RowNames = measuredRunSet.Properties.RowNames;
        delta.Properties.VariableNames= measuredRunSet{1, 1}{1, 1}.Properties.VariableNames;


        % compute the discriminative power for each iteration
        for k = 1:iterations

            % compute the discriminative power for each sample
            for s = 1:sampleNum

                % the original pool is present only in the first column of the
                % first iteration, skip senseless computations afterwards
                if s == 1 && k > 1
                    asl{k, s} = asl{1, 1};
                    dp{k, s} = dp{1, 1};
                    delta{k, s} = delta{1, 1};
                    continue;
                end;

                % prepare the input to discriminative power by extracting the
                % measured run sets for a given pool sample and iteration


                tmp = measuredRunSet{k, 1}{1, 1}{:, s};
                tmp = vertcat(tmp{:, :});

                % the name is the short name of the measure plus the sample in
                % order to differentiate, e.g., between AP at pool sample 100%
                % and AP at pool sample 50%
                tmp.Properties.UserData.shortName = [measuredRunSet{k, 1}{1, 1}.Properties.UserData.shortName '_' ...
                    measuredRunSet{k, 1}{1, 1}.Properties.VariableNames{s}];

                input={tmp};


                % in the first iteration, we need to check whether we have
                % input replicates or we need to obtain them from discriminative
                % power for the first time, while in the following iterations
                % we will always use the already existing replicates
                if s == 1 && k == 1
                    if supplied.Replicates
                        [currentAsl, currentDp, currentDelta] = discriminativePower(input{:}, 'Replicates', replicates, otherArgs{:});
                    else
                        [currentAsl, currentDp, currentDelta, replicates] = discriminativePower(input{:}, otherArgs{:});
                    end;

                    % set also some general properties we do not know until the
                    % first run of discriminative power
                    asl.Properties.UserData.method = currentAsl.Properties.UserData.method;
                    asl.Properties.UserData.shortMethod = currentAsl.Properties.UserData.shortMethod;
                    asl.Properties.UserData.alpha = currentAsl.Properties.UserData.alpha;
                    asl.Properties.UserData.replicates = currentAsl.Properties.UserData.replicates;

                    dp.Properties.UserData.method = currentDp.Properties.UserData.method;
                    dp.Properties.UserData.shortMethod = currentDp.Properties.UserData.shortMethod;
                    dp.Properties.UserData.alpha = currentDp.Properties.UserData.alpha;
                    dp.Properties.UserData.replicates = currentDp.Properties.UserData.replicates;

                    delta.Properties.UserData.method = currentDelta.Properties.UserData.method;
                    delta.Properties.UserData.shortMethod = currentDelta.Properties.UserData.shortMethod;
                    delta.Properties.UserData.alpha = currentDelta.Properties.UserData.alpha;
                    delta.Properties.UserData.replicates = currentDelta.Properties.UserData.replicates;

                else
                    [currentAsl, currentDp, currentDelta] = discriminativePower(input{:}, 'Replicates', replicates, otherArgs{:});
                end;

                asl{k, s} = currentAsl{1,1};
                dp{k, s} = currentDp{:,:};
                delta{k, s} = currentDelta{:,:};

            end;
        end
        
        corrID = EXPERIMENT.pattern.identifier.downsampledDPower(measureLabel{m}, trackID);

        aslID = EXPERIMENT.pattern.identifier.pmMeasure('asl', measureLabel{m}, trackID);
        dpID = EXPERIMENT.pattern.identifier.pmMeasure('dp', measureLabel{m}, trackID);
        deltaID = EXPERIMENT.pattern.identifier.pmMeasure('delta', measureLabel{m}, trackID);
               
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
            'WorkspaceVarNames', {'asl','dp','delta'}, ...
            'FileVarNames', {aslID,dpID,deltaID});
        
        clear('measuredRunSet','asl','dp','delta');
    end;
    fprintf('\n\n######## Total elapsed time for computing discriminative power on samples of pool on track %s (%s):\n %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;

end

%%
