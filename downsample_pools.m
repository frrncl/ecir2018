%% import_downsampled_pools
% 
% Imports the requested collection and saves it to a |.mat| file.
% Downsamples the pools of the collection, and saves them to a 
% |.mat| file.
%
%% Synopsis
%
%   [] = import_downsampled_pools(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to import.
%
% *Returns*
%
% Nothing
%
%%
function [] = downsample_pools(trackID)

    % check the number of input arguments
    narginchk(1, 1);

    % set up the common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    % disable warnings, not needed for bulk import
    %warning('off');
    
    % start of overall import
    startImport = tic;
    
    fprintf('\n\n######## Computing downsampled pools ########\n\n');

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));

    iterations = 1;

    % start of overall computations
    start = tic;
    fprintf('+ Loading the dataset\n');
    
    poolID = EXPERIMENT.pattern.identifier.pool(trackID);
    serload2(EXPERIMENT.pattern.file.dataset(trackID, poolID), ...
        'WorkspaceVarNames', {'pool'}, ...
        'FileVarNames', {poolID});
    
    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));   

    start = tic;
    fprintf('  - downsampling the pool at %s over %d iterations\n', ...
                num2str(EXPERIMENT.track.poolReductionRates, '%d%% '), iterations);            
    
    downsampled_PoolID = EXPERIMENT.pattern.identifier.downsampledPool(trackID);
    downsampled_Pool = downsamplePool(pool, [], ...
            'Downsampling', 'StratifiedRandomSampling', ...
            'SampleSize', EXPERIMENT.track.poolReductionRates, 'Iterations', iterations);
            
    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    start = tic;
    fprintf('  - saving the downsampled pools\n');
    
    sersave2(EXPERIMENT.pattern.file.dataset(trackID, downsampled_PoolID), ...
                'WorkspaceVarNames', {'downsampled_Pool'}, ...
                'FileVarNames', {downsampled_PoolID});
    
    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
    
    fprintf('\n\n######## Total elapsed time for importing collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startImport)));

    % enable warnings
    warning('on');