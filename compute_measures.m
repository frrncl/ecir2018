%% compute_measures
%
% Computes measures for the split of the given track and saves them to a
% |.mat| file.
%
%% Synopsis
%
%   [] = compute_measures(trackID, pListID)
%
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|pListID |* - the identifier of the probability list to process, if any.
%
% *Returns*
%
% Nothing
%

%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_measures(trackID, pListID)

    % check the number of input arguments
    narginchk(2, 2);

    % setup common parameters
    common_parameters;

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    
    % check that pListID is a non-empty string
    validateattributes(pListID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'pListID');

    if iscell(pListID)
        % check that pListID is a cell array of strings with one element
        assert(iscellstr(pListID) && numel(pListID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected pListID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    pListID = char(strtrim(pListID));
    pListID = pListID(:).';
    
     % check that pListID assumes a valid value
    validatestring(pListID, ...
        fieldnames(EXPERIMENT.measure.erap.probability), '', 'pListID');

    startMeasure = 1;
    endMeasure = EXPERIMENT.measure.number;

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing measures on track %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - slice \n');
    fprintf('    * start measure: %d\n', startMeasure);
    fprintf('    * end measure: %d\n', endMeasure);


    fprintf('+ Loading the dataset\n');

    poolID = EXPERIMENT.pattern.identifier.pool(trackID);
    serload2(EXPERIMENT.pattern.file.dataset(trackID, poolID), ...
        'WorkspaceVarNames', {'pool'}, ...
        'FileVarNames', {poolID});
    
    runID = EXPERIMENT.pattern.identifier.run(trackID);
    serload2(EXPERIMENT.pattern.file.dataset(trackID, runID), ...
        'WorkspaceVarNames', {'runSet'}, ...
        'FileVarNames', {runID});


    % for each measure
    for m = startMeasure:endMeasure

        start = tic;

        fprintf('+ Computing %s\n', EXPERIMENT.measure.getAcronym(m));

        mid = EXPERIMENT.measure.list{m};

        if isfield(EXPERIMENT.measure.(mid),'probability')
            
            pList = EXPERIMENT.measure.(mid).probability.(pListID);
            
            for p = 1 : length(pList)
                
                pid = pList{p};
                
                measure = EXPERIMENT.measure.(mid).compute(pool, runSet, trackID, pid);
                
                measureID = EXPERIMENT.pattern.identifier.measure(EXPERIMENT.pattern.identifier.pid(mid, pid), trackID);
                
                sersave2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                    'WorkspaceVarNames', {'measure'}, ...
                    'FileVarNames', {measureID});
                
                clear measure;
            end
            
        else
            
            measure = EXPERIMENT.measure.(mid).compute(pool, runSet, trackID);
            
            measureID = EXPERIMENT.pattern.identifier.measure(mid, trackID);
            
            sersave2(EXPERIMENT.pattern.file.measure(trackID, measureID), ...
                'WorkspaceVarNames', {'measure'}, ...
                'FileVarNames', {measureID});
            
            clear measure;
            
        end

        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    end; % for measure


    fprintf('\n\n######## Total elapsed time for computing measures on track %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
