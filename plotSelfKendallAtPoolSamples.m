%% plotSelfKendallAtPoolSamples
% 
% It plots the Kendall's tau B correlation between the measure on the 
% original pool and the measure on the sampled pool for only one
% iteration.
%
%% Synopsis
%
%   plotSelfKendallAtPoolSamples(varargin)
%  
%
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the 
% argument name and |Value| is the corresponding value. |Name| must appear 
% inside single quotes (' '). You can specify several name and value pair 
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% * *|Observed|* (mandatory) - a cell vector containing the acronym of each 
% observed measure for which the self kendal correlation has already been
% computed.
% * *|TrackID|* (mandatory) - the identifier of the track to process.
% * *|Reference|* (optional) - a cell vector containing the acronym of each 
% measure of reference for which the self kendal correlation has already been
% computed.
% * *|OutputPath|* (optional) - a string specifying the path to the output 
% directory where the PDF of the plot will be saved. If not specified, the
% plot will not be saved to a PDF.
% * *|FileNameSuffix|* (optional) - a string to be added in the file name 
% for saving the PDF of the plot.
%
%% Example of use
%  
%   plotSelfKendallAtPoolSamples('Observed', {'P10', 'P100', 'R_PREC'}, ...
%               'Reference', {'AP','RBP_080'},...
%               'TtrackID', 'T13',...
%               'OutputPath', '/output')
%
% You can note as |Observed| performances are plotted with a
% continuous line with a while |Reference| performance are plotted in black with a
% dashed line.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% <mailto:silvello@dei.unipd.it Gianmaria Silvello>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2013-2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = plotSelfKendallAtPoolSamples(varargin)
    
    common_parameters
    
    persistent MARKERS ...
        REF_LINESTYLE REF_LINEWIDTH REF_MARKERSIZE ...
        OBS_LINESTYLE OBS_LINEWIDTH OBS_MARKERSIZE;
    
    if isempty(MARKERS)
        
        %MARKERS = {'o', '+', '*', 'x', 's', 'd', '^', 'v', '>', '<', 'p', 'h'};
        
        MARKERS = {'^', '+', '*', 'v', 'x', 'd', 'o', 's', '>', 'p', 'x', 'h'};

        REF_LINESTYLE = '--';
        REF_LINEWIDTH = 2.0;
        REF_MARKERSIZE = 6;
        
        OBS_LINESTYLE = '-';
        OBS_LINEWIDTH = 3;
        OBS_MARKERSIZE = 7;        
    end;
       
    % check that we have the correct number of input arguments. 
    narginchk(1, inf);
    
    % parse the variable inputs
    pnames = {'OutputPath'  'Observed', 'Reference', 'FileNameSuffix', 'TrackID'};
    dflts =  {[]             []         []             ''                ''};
    
    if verLessThan('matlab', '9.2.0')
        [outputPath, observed, reference, fileNameSuffix, trackID, supplied] = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [outputPath, observed, reference, fileNameSuffix, trackID, supplied] = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end
    
    
     if supplied.OutputPath
        % check that path is a non-empty string
        validateattributes(outputPath, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'OutputPath');
        
        if iscell(outputPath)
            % check that path is a cell array of strings with one element
            assert(iscellstr(outputPath) && numel(outputPathpath) == 1, ...
                'MATTERS:IllegalArgument', 'Expected OutputPath to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        outputPath = char(strtrim(outputPath));
        outputPath = outputPath(:).';

        % check if the path is a directory and if it exists
        if ~(isdir(outputPath))
            error('MATTERS:IllegalArgument', 'Expected OutputPath to be a directory.');
        end;

        % check if the given directory path has the correct separator at the
        % end.
        if outputPath(end) ~= filesep;
           outputPath(end + 1) = filesep;
        end; 
    end;
        
    if supplied.FileNameSuffix
        if iscell(fileNameSuffix)
            % check that fileNameSuffix is a cell array of strings with one element
            assert(iscellstr(fileNameSuffix) && numel(fileNameSuffix) == 1, ...
                'MATTERS:IllegalArgument', 'Expected FileNameSuffix to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        fileNameSuffix = char(strtrim(fileNameSuffix));
        fileNameSuffix = fileNameSuffix(:).';
        
        % check that the nameSuffix is ok according to the matlab rules
        if ~isempty(regexp(fileNameSuffix, '\W*', 'once'))
            error('MATTERS:IllegalArgument', 'FileNameSuffix %s is not valid: it can contain only letters, numbers, and the underscore.', ...
                fileNameSuffix);
        end  
    
    end;
    
    if supplied.TrackID
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');
        
        if iscell(trackID)
            
            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';
        
        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.track.list, '', 'trackID');
    else
        error('MATTERS:MissingArgument', 'Parameter ''Observed'' not provided: the observed performances are mandatory.');
    end;
    
    if supplied.Observed
        % check that observed is a non-empty char vector
        validateattributes(observed, {'cell'}, {'nonempty', 'vector'}, '', 'Observed');
        
        for k = 1:length(observed)
            % check that each element is a non-empty table
            validateattributes(observed{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Observed{%d}'), 1);
            observed{k} = char(strtrim(observed{k}));
        end;
    else
        error('MATTERS:MissingArgument', 'Parameter ''Observed'' not provided: the observed performances are mandatory.');
    end;
    
    if supplied.Reference
        % check that reference is a non-empty cell vector
        validateattributes(reference, {'cell'}, {'nonempty', 'vector'}, '', 'Reference');

        for k = 1:length(reference)
            % check that each element is a non-empty table
            validateattributes(reference{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Reference{%d}'), 1);
            
        end;        
        
        % do not use more than half of the markers for the reference
        % performances
        refMarkers = MARKERS(1:min(length(reference), length(MARKERS)/2));
        
        % assign all the remaining markers for the observed performances
        obsMarkers = MARKERS(length(refMarkers)+1:end);
        
    end;
    
    % the labels for the legend, as deduced from the short name of the
    % computed measure
    legendLabels = observed;
    % remove useless white spaces from the title, if any, and ensure it is a char row
    plotTitle = char(strtrim(EXPERIMENT.track.(trackID).name));
    plotTitle = plotTitle(:).';
    plotTitle = strrep(plotTitle, '_', '\_');
    
    selfCorr=cell(length(observed),1);
    for k=1:length(observed)
        
            corrID = EXPERIMENT.pattern.identifier.downsampledSelfKendal(observed{k}, trackID);
            serload2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
                'WorkspaceVarNames', {'sCorr'}, ...
                'FileVarNames', {corrID});
            selfCorr{k}=sCorr;
            if (k == 1 && sCorr.Properties.UserData.iterations ~= 1)
                error('MATTERS:IllegalArgument', 'Expected number of iterations to be equal to 1.');
            end;
    end
    
    % markers for the observed performances
    obsMarkers = MARKERS;
        
    if supplied.Reference
        
        selfCorrRef=cell(length(reference),1);
        legendLabels = [legendLabels reference];
        
        for k = 1:length(reference)
            
            corrID = EXPERIMENT.pattern.identifier.downsampledSelfKendal(reference{k}, trackID);
            serload2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
                'WorkspaceVarNames', {'sCorr'}, ...
                'FileVarNames', {corrID});
            selfCorrRef{k}=sCorr;
            if (k == 1 && sCorr.Properties.UserData.iterations ~= 1)
                error('MATTERS:IllegalArgument', 'Expected number of iterations to be equal to 1.');
            end;
        end;        
        
        % do not use more than half of the markers for the reference
        % performances
        refMarkers = MARKERS(1:min(length(reference), length(MARKERS)/2));
        
        % assign all the remaining markers for the observed performances
        obsMarkers = MARKERS(length(refMarkers)+1:end);
        
    end;
            
    switch lower(selfCorr{1}.Properties.UserData.shortDownsampling)
        case 'srs'
            xTickLabels = [100 selfCorr{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Kendall''s \tau Correlation with Stratified Random Pool Sampling  '};
        case 'rs'
            xTickLabels = [100 selfCorr{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Kendall''s \tau Correlation with Random Pool Sampling  '};
        case 'pds'
            xTickLabels = [100 selfCorr{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Kendall''s \tau Correlation with Pool Depth Sampling  '};
        otherwise
            error('MATTERS:IllegalArgument', 'Unrecognized pool downsampling method %s. Only PoolDepthSampling, RandomSampling, and StratifiedRandomSampling are allowed', ...
                selfCorr{1}.Properties.UserData.downsampling);
    end;
        

    
    
  
    legendLabels = strrep(legendLabels, '_', '\_');
    obsColors = winter(length(observed));
    refColors = bone(length(reference)+2);


    % if output path is supplied, hide the figure to avoid render it on
    % screen when it is actually saved to a file
    if supplied.OutputPath
        h = figure('Visible', 'off');
    else
        h = figure;
    end;
    hold on
        
        % plot the observed performances
        for k = 1:length(observed)
            
            tmp = selfCorr{k}{:, :};
            plot(tmp, 'Color', obsColors(k, :), 'LineStyle', OBS_LINESTYLE, ...
                'Marker', obsMarkers{mod(k, length(obsMarkers)) + 1}, ...
                'LineWidth', OBS_LINEWIDTH, 'MarkerSize', OBS_MARKERSIZE, ...
                'MarkerFaceColor', obsColors(k, :));
          
        end
        
                % plot the reference performances
        for k = 1:length(reference)
            tmp = selfCorrRef{k}{:, :};  
            plot(tmp, 'Color', refColors(k, :), 'LineStyle', REF_LINESTYLE, ...
                'Marker', refMarkers{mod(k, length(refMarkers)) + 1}, ...
                'LineWidth', REF_LINEWIDTH, 'MarkerSize', REF_MARKERSIZE, ...
                'MarkerFaceColor', refColors(k, :));
      
        end
        
        set(gca, 'FontSize', 14);
        set(gca,'XTick', 1:length(xTickLabels));
        set(gca,'XTickLabel', xTickLabels);
        
        set(gca, 'YLim', [0 1], ...
            'YTick', [0:0.10:1], ...
            'YTickLabel', cellstr(num2str([0:0.10:1].', '%3.2f')).');
        
        grid on;
        
        hl = legend(legendLabels{:}, 'Location', 'SouthWest');
        set(hl, 'FontSize', 12);

        xlabel(xLabel)
        ylabel('Kendall''s \tau Correlation')
        title(plotTitle);

        if ~isempty(outputPath)
            set(h,'PaperPositionMode','manual'); 
            set(h,'PaperUnits','normalized');
            set(h,'PaperPosition',[0.05 0.05 0.9 0.9]);
            set(h,'PaperType','A4');
            set(h,'PaperOrientation','landscape');
            print(h, '-dpdf', sprintf('%sselfTauB_AtPoolSamples%s_%s.pdf', outputPath, fileNameSuffix, trackID));
            close(h);
        end;
        
end



