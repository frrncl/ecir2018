%% analyse_base_measures
%
% Analyses the base measures at different k-uples sizes and saves
% them to a |.mat| file.
%
%% Synopsis
%
%   [] = analyse_crowd_measures(trackID,pListID)
%
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|pListID |* - the identifier of the probability list to process, if any.
% 
%
% *Returns*
%
% Nothing
%

%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = analyse_crowd_measures(trackID, pListID)

    % check the number of input arguments
    narginchk(2, inf);

    % setup common parameters
    common_parameters;

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');

    % check that pListID is a non-empty string
    validateattributes(pListID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'pListID');

    if iscell(pListID)
        % check that pListID is a cell array of strings with one element
        assert(iscellstr(pListID) && numel(pListID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected pListID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    pListID = char(strtrim(pListID));
    pListID = pListID(:).';

    % check that pListID assumes a valid value
    validatestring(pListID, ...
        fieldnames(EXPERIMENT.measure.gap.probability), '', 'pListID');

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Analysing measures on collection %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - confidence interval alpha %f\n', EXPERIMENT.analysis.ciAlpha);
    fprintf('  - AP correlation ties samples %d\n', EXPERIMENT.analysis.apcorrTiesSamples);

    % load data
    serload(EXPERIMENT.pattern.file.crowdDataset(trackID, trackID), 'poolLabels', 'poolIdentifiers', 'T', 'P');

    % for each runset
    for r = 1:EXPERIMENT.track.(trackID).runSet.originalTrackID.number

        fprintf('\n+ Original track of the runs: %s\n', EXPERIMENT.track.(trackID).runSet.originalTrackID.id{r});

        % for each measure
        for m = 1:EXPERIMENT.measure.number

            start = tic;


            mid = EXPERIMENT.measure.list{m};

            fprintf('  - analysing %s\n', mid);


            if isfield(EXPERIMENT.measure.(mid),'probability')

                pList = EXPERIMENT.measure.(mid).probability.(pListID);

                for prob = 1 : length(pList)

                    pid = pList{prob};

                    % loading the gold standard measure
                    goldMeasureID = EXPERIMENT.pattern.identifier.crowdMeasure(EXPERIMENT.pattern.identifier.pid(mid, pid), EXPERIMENT.label.goldPool, ...
                        trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                    fprintf('    * loading gold standard measure: %s\n', goldMeasureID);

                    serload(EXPERIMENT.pattern.file.crowdMeasure(trackID, EXPERIMENT.pattern.identifier.measure(EXPERIMENT.pattern.identifier.pid(mid, pid), trackID))  , goldMeasureID);

                    % determine the total number of runs
                    evalf(@width, {goldMeasureID}, {'R'})

                    % the raw data, each TxR plane is a measure for T topics and R runs
                    % to be weighted by assessors' scores and the P planes correspond
                    % to the different assessors
                    data = NaN(T, R, P);

                    fprintf('    * loading assessor measures\n');
                    % for each pool
                    for p = 1:P

                        assessorMeasureID = EXPERIMENT.pattern.identifier.crowdMeasure(EXPERIMENT.pattern.identifier.pid(mid, pid), poolLabels{p}, ...
                            trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                        serload(EXPERIMENT.pattern.file.crowdMeasure(trackID, EXPERIMENT.pattern.identifier.measure(EXPERIMENT.pattern.identifier.pid(mid, pid), trackID)), assessorMeasureID);

                        eval(sprintf('data(:, :, p) = %1$s{:, :};', assessorMeasureID));

                        clear(assessorMeasureID);
                    end;
                    clear assessorMeasureID;

                    fprintf('    * analysing measures\n');

                    measureID = EXPERIMENT.pattern.identifier.crowdMeasure(EXPERIMENT.pattern.identifier.pid(mid, pid), EXPERIMENT.label.goldPool, ...
                        trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                    tauID = EXPERIMENT.pattern.identifier.pm('tau', EXPERIMENT.pattern.identifier.pid(mid, pid), EXPERIMENT.label.goldPool, ...
                        trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                    apcID = EXPERIMENT.pattern.identifier.pm('apc', EXPERIMENT.pattern.identifier.pid(mid, pid), EXPERIMENT.label.goldPool, ...
                        trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                    rmseID = EXPERIMENT.pattern.identifier.pm('rmse', EXPERIMENT.pattern.identifier.pid(mid, pid), EXPERIMENT.label.goldPool, ...
                        trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});


                    evalf(EXPERIMENT.command.analyse, ...
                        {goldMeasureID, 'data'}, ...
                        {rmseID, tauID, apcID});

                    sersave(EXPERIMENT.pattern.file.analysis(trackID, measureID), tauID(:), apcID(:), rmseID(:));

                    clear(goldMeasureID, tauID, apcID, rmseID);

                    clear data;

                    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                end

            else
                % loading the gold standard measure
                goldMeasureID = EXPERIMENT.pattern.identifier.crowdMeasure(mid, EXPERIMENT.label.goldPool, ...
                    trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                fprintf('    * loading gold standard measure: %s\n', goldMeasureID);

                serload(EXPERIMENT.pattern.file.crowdMeasure(trackID, EXPERIMENT.pattern.identifier.measure(mid, trackID))  , goldMeasureID);

                % determine the total number of runs
                evalf(@width, {goldMeasureID}, {'R'})

                % the raw data, each TxR plane is a measure for T topics and R runs
                % to be weighted by assessors' scores and the P planes correspond
                % to the different assessors
                data = NaN(T, R, P);

                fprintf('    * loading assessor measures\n');
                % for each pool
                for p = 1:P

                    assessorMeasureID = EXPERIMENT.pattern.identifier.crowdMeasure(mid, poolLabels{p}, ...
                        trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                    serload(EXPERIMENT.pattern.file.crowdMeasure(trackID, EXPERIMENT.pattern.identifier.measure(mid, trackID)), assessorMeasureID);

                    eval(sprintf('data(:, :, p) = %1$s{:, :};', assessorMeasureID));

                    clear(assessorMeasureID);
                end;
                clear assessorMeasureID;

                fprintf('    * analysing measures\n');

                measureID = EXPERIMENT.pattern.identifier.crowdMeasure(mid, EXPERIMENT.label.goldPool, ...
                    trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                tauID = EXPERIMENT.pattern.identifier.pm('tau', mid, EXPERIMENT.label.goldPool, ...
                    trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                apcID = EXPERIMENT.pattern.identifier.pm('apc', mid, EXPERIMENT.label.goldPool, ...
                    trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                rmseID = EXPERIMENT.pattern.identifier.pm('rmse', mid, EXPERIMENT.label.goldPool, ...
                    trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});


                evalf(EXPERIMENT.command.analyse, ...
                    {goldMeasureID, 'data'}, ...
                    {rmseID, tauID, apcID});

                sersave(EXPERIMENT.pattern.file.analysis(trackID, measureID), tauID(:), apcID(:), rmseID(:));

                clear(goldMeasureID, tauID, apcID, rmseID);

                clear data;

                fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
            end

        end; % for each measure

    end; % for each run set


    fprintf('\n\n######## Total elapsed time for analysing measures on collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

end
