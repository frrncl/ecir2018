%% computeSelfKendallAtPoolSamples
% 
% Load the measures computed among the downsampled pools. For each iteration and pool sample, 
% it computes the Kendall's tau B correlation between the measure on the orginal pool 
% and the measure on the sampled pool and saves it to a |.mat| file.
%
%% Synopsis
%
%   [] = computeSelfKendallAtPoolSamples(measuredRunSet, varargin)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|pListID |* - the identifier of the probability list to process, if any.
%
% *Returns*
%
% Nothing
%
%
%% Variables to be saved
%
% * *|tauB|*  - is a table containing a row for each iteration and a 
% column for each pool sample. The value of each cell is the Kendall's tau B
% between the measure computed on the original pool and the measure
% computed on the given pool sample at the given iteration.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% <mailto:silvello@dei.unipd.it Gianmaria Silvello>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2013-2014 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = computeSelfKendallAtPoolSamples(trackID, pListID)

   
%     iterations = 1;

    % check that we have the correct number of input arguments.
    narginchk(2, Inf);

    % setup common parameters
    common_parameters;

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
     % check that pListID is a non-empty string
    validateattributes(pListID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'pListID');

    if iscell(pListID)
        % check that pListID is a cell array of strings with one element
        assert(iscellstr(pListID) && numel(pListID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected pListID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    pListID = char(strtrim(pListID));
    pListID = pListID(:).';
    
     % check that pListID assumes a valid value
    validatestring(pListID, ...
        fieldnames(EXPERIMENT.measure.gap.probability), '', 'pListID');

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing correlation(s)  on collection %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


    measureIDList = [];
    measureLabel = [];

    for m = 1: EXPERIMENT.measure.number

        mid = EXPERIMENT.measure.list{m};

        if isfield(EXPERIMENT.measure.(mid),'probability')

            pList = EXPERIMENT.measure.(mid).probability.(pListID);

            for p = 1 : length(pList)

                pid = pList{p};

                measureIDList = [measureIDList {EXPERIMENT.pattern.identifier.downsampledMeasure(EXPERIMENT.pattern.identifier.pid(mid, pid), trackID)}];

                measureLabel = [measureLabel {EXPERIMENT.pattern.identifier.pid(EXPERIMENT.measure.getAcronym(m), pid)}];

            end

        else
            measureIDList = [measureIDList {EXPERIMENT.pattern.identifier.downsampledMeasure(mid, trackID)}];

            measureLabel = [measureLabel {EXPERIMENT.measure.getAcronym(m)}];

        end;
    end

    measureNumber = length(measureIDList);


    fprintf('+ Loading measures\n');

    measures = [];

    % for each measure
    for m = 1:measureNumber
        
        
        fprintf('+ loading %s\n', measureIDList{m});

        % load the measure in the tmp variable
        serload2(EXPERIMENT.pattern.file.measure(trackID, measureIDList{m}), ...
            'WorkspaceVarNames', {'measuredRunSet'}, ...
            'FileVarNames', {measureIDList{m}});

        iterations = measuredRunSet.Properties.UserData.iterations;
       
        % check that measured run set is a non-empty table
        validateattributes(measuredRunSet, {'table'}, {'nonempty'}, '', 'measuredRunSet', 1);

        sampleNum = length(measuredRunSet.Properties.UserData.sampleSize) + 1;


        % the Kendall's tau B at different iterations
        tauB = array2table(NaN(iterations, sampleNum));
        tauB.Properties.UserData.identifier = measuredRunSet.Properties.UserData.identifier;
        tauB.Properties.UserData.pool = measuredRunSet.Properties.UserData.pool;
        tauB.Properties.UserData.funcName = measuredRunSet.Properties.UserData.funcName;
        tauB.Properties.UserData.downsampling = measuredRunSet.Properties.UserData.downsampling;
        tauB.Properties.UserData.shortDownsampling = measuredRunSet.Properties.UserData.shortDownsampling;
        tauB.Properties.UserData.name = measuredRunSet.Properties.UserData.name;
        tauB.Properties.UserData.shortName = measuredRunSet.Properties.UserData.shortName;
        tauB.Properties.UserData.sampleSize = measuredRunSet.Properties.UserData.sampleSize;
        tauB.Properties.UserData.iterations = measuredRunSet.Properties.UserData.iterations;
        tauB.Properties.RowNames = measuredRunSet.Properties.RowNames;
        tauB.Properties.VariableNames= measuredRunSet{1, 1}{1, 1}.Properties.VariableNames;


        % the measure computed on the original pool
        original = measuredRunSet{1, 1}{1, 1}{:, 1};
        original = vertcat(original{:, :});
        original.Properties.UserData.shortName = [measuredRunSet{1, 1}{1, 1}.Properties.UserData.shortName '_original'];

        % compute the Kendall's tau B for each iteration
        for k = 1:iterations

            % compute the Kendall's tau B for each sample
            for s = 2:sampleNum

                % the measure computed on the sampled pool
                tmp = measuredRunSet{k, 1}{1, 1}{:, s};
                tmp = vertcat(tmp{:, :});

                % the name is the short name of the measure plus the sample in
                % order to differentiate, e.g., between AP at pool sample 100%
                % and AP at pool sample 50%
                tmp.Properties.UserData.shortName = [measuredRunSet{k, 1}{1, 1}.Properties.UserData.shortName '_' ...
                    measuredRunSet{k, 1}{1, 1}.Properties.VariableNames{s}];

                tau = kendall(original, tmp);

                tauB{k, s} = tau{1, 2};
            end;
        end;

        clear('measuredRunSet');
        
        tauB{:, 1} = 1;

        corrID = EXPERIMENT.pattern.identifier.downsampledSelfKendal(measureLabel{m}, trackID);

        sersave2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
            'WorkspaceVarNames', {'tauB'}, ...
            'FileVarNames', {corrID});
    end;
    
    fprintf('\n\n######## Total elapsed time for computing self Kendall correlation on samples of pool on track %s (%s):\n %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
    
end



