%% compute_crowd_measures
%
% Computes measures for the crowed pools and the gold one and saves them to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_crowd_measures(trackID, pListID)
%
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|pListID |* - the identifier of the probability list to process, if any.
%
% *Returns*
%
% Nothing
%
%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_crowd_measures(trackID, pListID)

    % check the number of input arguments
    narginchk(2, 2);

    % setup common parameters
    common_parameters;

    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');


    % check that pListID is a non-empty string
    validateattributes(pListID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'pListID');

    if iscell(pListID)
        % check that pListID is a cell array of strings with one element
        assert(iscellstr(pListID) && numel(pListID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected pListID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    pListID = char(strtrim(pListID));
    pListID = pListID(:).';

    % check that pListID assumes a valid value
    validatestring(pListID, ...
        fieldnames(EXPERIMENT.measure.gap.probability), '', 'pListID');

    startMeasure = 1;
    endMeasure = EXPERIMENT.measure.number;

    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing measures on collection %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));


    start = tic;
    fprintf('  - loading the dataset\n');

    serload(EXPERIMENT.pattern.file.crowdDataset(trackID, trackID));

    fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

    % Adding the gold pool label and identifier to run everything in one cycle
    poolLabels = [{EXPERIMENT.label.goldPool} poolLabels];
    poolIdentifiers = [{ EXPERIMENT.pattern.identifier.goldPool(trackID)} poolIdentifiers];

    % for each pool
    for p = 1:length(poolLabels)

        startPool = tic;

        fprintf('\n+ Pool: %s\n', poolIdentifiers{p});

        % for each runset
        for r = 1:EXPERIMENT.track.(trackID).runSet.originalTrackID.number

            fprintf('  - original track of the runs: %s\n', EXPERIMENT.track.(trackID).runSet.originalTrackID.id{r});

            % for each measure
            for m = startMeasure:endMeasure

                start = tic;

                fprintf('    * computing %s\n', EXPERIMENT.measure.getAcronym(m));

                mid = EXPERIMENT.measure.list{m};

                if isfield(EXPERIMENT.measure.(mid),'probability')

                    pList = EXPERIMENT.measure.(mid).probability.(pListID);

                    for prob = 1 : length(pList)

                        pid = pList{prob};

                        measureID = EXPERIMENT.pattern.identifier.crowdMeasure(EXPERIMENT.pattern.identifier.pid(mid, pid), poolLabels{p}, ...
                            trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});
                        
                        shortNameSuffix = poolLabels{p};
                        evalf(EXPERIMENT.measure.(mid).compute, ...
                            {poolIdentifiers{p}, EXPERIMENT.pattern.identifier.runSet(trackID, EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r}), 'shortNameSuffix','pid'}, ...
                            {measureID});

                        sersave(EXPERIMENT.pattern.file.crowdMeasure(trackID, EXPERIMENT.pattern.identifier.measure(EXPERIMENT.pattern.identifier.pid(mid, pid), trackID)), measureID(:));
                                                                                            
                        % free space
                        clear(measureID);

                    end

                else
                    
                    measureID = EXPERIMENT.pattern.identifier.crowdMeasure(mid, poolLabels{p}, ...
                        trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});

                    shortNameSuffix = poolLabels{p};
                    evalf(EXPERIMENT.measure.(mid).compute, ...
                        {poolIdentifiers{p}, EXPERIMENT.pattern.identifier.runSet(trackID, EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r}), 'shortNameSuffix'}, ...
                        {measureID});

                    sersave(EXPERIMENT.pattern.file.crowdMeasure(trackID, EXPERIMENT.pattern.identifier.measure(mid, trackID)), measureID(:));

                    clear measureID;

                end


                fprintf('    * elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));

            end; % for measures

            % free the assess cache
            clear assess

        end; % for runset

        fprintf('  - elapsed time for pool %s: %s\n', poolIdentifiers{p}, elapsedToHourMinutesSeconds(toc(startPool)));

    end; % for pools

    fprintf('\n\n######## Total elapsed time for computing measures on collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

    diary off;
end
