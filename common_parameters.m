%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))
    addpath('/nas1/promise/ims/ferro/matters/base/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/analysis/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/io/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/measure/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/plot/')
    addpath('/nas1/promise/ims/ferro/matters/base/core/util/')
end

EXPERIMENT.machine.silvia = false;


% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
    EXPERIMENT.path.base = '/nas1/promise/ims/ferro/Sanderson/experiment/';
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2018/ECIR2018/experiment/';
end


% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'ECIR 2018 FFP';

% Label to be used for the gold standard pool
EXPERIMENT.label.goldPool = 'GOLD';

%% Configuration for Tracks

EXPERIMENT.track.poolReductionRates = [90 70 50 30 10 5];

EXPERIMENT.track.list = {'T13','T14','T15','T21','CrowdT21'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);

% TREC_04 Database
EXPERIMENT.track.T13.id = 'T13';
EXPERIMENT.track.T13.name = 'TREC 13 2004 Terabyte';
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T13.pool.file = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_13_2004_Terabyte\pool\qrels.2004.Terabyte.txt';
else
    EXPERIMENT.track.T13.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Terabyte/pool/qrels.2004.Terabyte.txt';
end
EXPERIMENT.track.T13.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T13.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T13.pool.delimiter = 'space';
EXPERIMENT.track.T13.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T13.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T13.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T13.pool.delimiter,  'Verbose', false);
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T13.run.path = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_13_2004_Terabyte\runs';
else
    EXPERIMENT.track.T13.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_13_2004_Terabyte/runs';
end
EXPERIMENT.track.T13.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T13.run.singlePrecision = true;
EXPERIMENT.track.T13.run.delimiter = 'tab';
EXPERIMENT.track.T13.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T13.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T13.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T13.run.delimiter, 'Verbose', false);

% TREC_05 Database
EXPERIMENT.track.T14.id = 'T14';
EXPERIMENT.track.T14.name = 'TREC 14 2005 Terabyte';
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T14.pool.file = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_14_2005_Terabyte\pool\terabyte.2005.adhoc_qrels.txt';
else
    EXPERIMENT.track.T14.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_14_2005_Terabyte/pool/terabyte.2005.adhoc_qrels.txt';
end
EXPERIMENT.track.T14.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T14.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T14.pool.delimiter = 'space';
EXPERIMENT.track.T14.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T14.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T14.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T14.pool.delimiter,  'Verbose', false);
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T14.run.path = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_14_2005_Terabyte\runs';
else
    EXPERIMENT.track.T14.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_14_2005_Terabyte/runs';
end
EXPERIMENT.track.T14.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T14.run.singlePrecision = true;
EXPERIMENT.track.T14.run.delimiter = 'tab';
EXPERIMENT.track.T14.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T14.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T14.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T14.run.delimiter, 'Verbose', false);

% TREC_06 Database
EXPERIMENT.track.T15.id = 'T15';
EXPERIMENT.track.T15.name = 'TREC 15 2006 Terabyte';
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T15.pool.file = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_15_2006_Terabyte\pool\qrels.tb06.top50.txt';
else
    EXPERIMENT.track.T15.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_15_2006_Terabyte/pool/qrels.tb06.top50.txt';
end
EXPERIMENT.track.T15.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T15.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T15.pool.delimiter = 'space';
EXPERIMENT.track.T15.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T15.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T15.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T15.pool.delimiter,  'Verbose', false);
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T15.run.path = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_15_2006_Terabyte\runs';
else
    EXPERIMENT.track.T15.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_15_2006_Terabyte/runs';
end
EXPERIMENT.track.T15.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T15.run.singlePrecision = true;
EXPERIMENT.track.T15.run.delimiter = 'tab';
EXPERIMENT.track.T15.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T15.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T15.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T15.run.delimiter, 'Verbose', false);

% TREC_12 Database
EXPERIMENT.track.T21.id = 'T21';
EXPERIMENT.track.T21.name = 'TREC 21 2012 Web';
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T21.pool.file = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_21_2012_Web\pool\qrels.adhoc.web.2012.txt';
else
    EXPERIMENT.track.T21.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_21_2012_Web/pool/qrels.adhoc.web.2012.txt';
end
EXPERIMENT.track.T21.pool.relevanceDegrees = {'NotRelevant','NotRelevant', 'PartiallyRelevant', 'Relevant', 'HighlyRelevant', 'HighlyRelevant'};
EXPERIMENT.track.T21.pool.relevanceGrades = [-2 0 1 2 3 4];
EXPERIMENT.track.T21.pool.delimiter = 'space';
EXPERIMENT.track.T21.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T21.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T21.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T21.pool.delimiter,  'Verbose', false);
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.T21.run.path = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_21_2012_Web\runs';
else
    EXPERIMENT.track.T21.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_21_2012_Web/runs/all';
end
EXPERIMENT.track.T21.run.documentOrdering = 'TrecEvalLexDesc';  
EXPERIMENT.track.T21.run.singlePrecision = true;
EXPERIMENT.track.T21.run.delimiter = 'tab';
EXPERIMENT.track.T21.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T21.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T21.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T21.run.delimiter, 'Verbose', false);


% Configuration for collection TREC 21, 2012, Crowdsourcing
% EXPERIMENT.track.CrowdT21.id = 'TREC_21_2012_Crowd';
EXPERIMENT.track.CrowdT21.id = 'CrowdT21';
EXPERIMENT.track.CrowdT21.name =  'TREC 21, 2012, Crowdsourcing';
if(EXPERIMENT.machine.silvia)
    EXPERIMENT.track.CrowdT21.path.base = 'C:\Users\Silvia92\Dropbox\Borsa DI Studio\Measurement\Simulazioni\Matters\TREC\TREC_21_2012_Crowd\';
    else
    EXPERIMENT.track.CrowdT21.path.base = '/Users/ferro/Documents/experimental-collections/TREC/TREC_21_2012_Crowd/';
end
EXPERIMENT.track.CrowdT21.path.crowd = sprintf('%1$s%2$s%3$s', EXPERIMENT.track.CrowdT21.path.base, 'crowd', filesep);
EXPERIMENT.track.CrowdT21.path.runSet = @(originalTrackID) sprintf('%1$s%2$s%3$s%4$s%3$s', EXPERIMENT.track.CrowdT21.path.base, 'runs', filesep, originalTrackID);
EXPERIMENT.track.CrowdT21.file.goldPool = sprintf('%1$s%2$s', EXPERIMENT.track.CrowdT21.path.base, 'trat-adjudicated-qrels.txt');

EXPERIMENT.track.CrowdT21.importGoldPool = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'Delimiter', 'space',  'Verbose', false);
EXPERIMENT.track.CrowdT21.importCrowdPool = @(fileName, id, requiredTopics) importCrowd2012Pool('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', {'NotRelevant', 'Relevant'}, 'RelevanceGrades', 0:1, 'RequiredTopics', requiredTopics, 'Delimiter', 'tab',  'Verbose', false);
EXPERIMENT.track.CrowdT21.importRunSet = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', 'TrecEvalLexDesc', 'SinglePrecision', true, 'Verbose', false);


EXPERIMENT.track.CrowdT21.runSet.originalTrackID.id = {'TREC_08_1999_AdHoc', 'TREC_13_2004_Robust'};%
EXPERIMENT.track.CrowdT21.runSet.originalTrackID.shortID = {'T08', 'T13'};%
EXPERIMENT.track.CrowdT21.runSet.originalTrackID.name = {'TREC 08, 1999, AdHoc', 'TREC 13, 2004, Robust'};%
EXPERIMENT.track.CrowdT21.runSet.originalTrackID.number = length(EXPERIMENT.track.CrowdT21.runSet.originalTrackID.id);


% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 

%% Patterns for file names

% MAT - Pattern <path>/<trackID>/<fileID>.<ext> 
EXPERIMENT.pattern.file.general = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);
 
% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, datasetID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% Pattern EXPERIMENT.path.base/dataset/<trackID>/dataset_<trackShortID>.mat
EXPERIMENT.pattern.file.crowdDataset = @(trackID, trackShortID) sprintf('%1$sdataset%2$s%3$s%2$sdataset_%4$s.mat', EXPERIMENT.path.base, filesep, trackID, trackShortID);

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure = @(trackID, measureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.measure, trackID, measureID, 'mat');


% Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>
EXPERIMENT.pattern.file.crowdMeasure = @(trackID, measureID) ...
    sprintf('%1$smeasure%2$s%3$s%2$s%4$s.mat', EXPERIMENT.path.base, filesep, trackID, measureID);

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis = @(trackID, analysisID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>
EXPERIMENT.pattern.file.report = @(trackID, reportID) EXPERIMENT.pattern.file.general(EXPERIMENT.path.report, trackID, reportID, 'tex');


%% Patterns for identifiers

% Pattern pool_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(trackID) sprintf('pool_%1$s', trackID);

% Pattern downsampledPool_<trackID>
EXPERIMENT.pattern.identifier.downsampledPool =  @(trackID) sprintf('downsampledPool_%1$s', trackID);

% Pattern pool_<poolLabel>_<trackID>
EXPERIMENT.pattern.identifier.poolCrowd =  @(poolLabel, trackID) sprintf('pool_%1$s_%2$s', poolLabel, trackID);

% Pattern pool_GOLD_<trackID>
EXPERIMENT.pattern.identifier.goldPool = @(trackID) EXPERIMENT.pattern.identifier.poolCrowd( EXPERIMENT.label.goldPool, trackID);

% Pattern run_<trackID>
EXPERIMENT.pattern.identifier.run = @(trackID) sprintf('run_%1$s', trackID);

% Pattern runSet_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.runSet = @(trackID, originalTrackID) sprintf('runSet_p%1$sr%2$s', trackID, originalTrackID);

% Pattern <measureID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(measureID, trackID) sprintf('%1$s_%2$s', measureID, trackID);

% Pattern <mid>_<pid>
EXPERIMENT.pattern.identifier.pid = @(mid, pid) sprintf('%s%s',mid, num2str(pid*100, '_%03.0f'));

% Pattern DS_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.downsampledMeasure =  @(measureID, trackID) sprintf('DS_%1$s_%2$s', measureID, trackID);

% Pattern <measureID>_<experimentTag>_<originalTrackID>
EXPERIMENT.pattern.identifier.crowdMeasure = @(measureID, experimentLabel, trackID, originalTrackID) ...
    sprintf('%1$s_%2$s_p%3$sr%4$s', lower(measureID), experimentLabel, trackID, originalTrackID);

% Pattern <correlationID>_<pListID>_<trackID>
EXPERIMENT.pattern.identifier.correlation = @(correlationID, pListID, trackID) sprintf('%1$s_%2$s_%3$s', correlationID, pListID, trackID);
EXPERIMENT.pattern.identifier.downsampledSelfKendal = @(measureID, trackID) sprintf('DS_SelfKendall_%1$s_%2$s%3$s', measureID, trackID);
EXPERIMENT.pattern.identifier.downsampledDPower = @(measureID, trackID) sprintf('DS_DPower_%1$s_%2$s%3$s', measureID, trackID);

EXPERIMENT.pattern.identifier.figure.general = @(measureID, trackID) sprintf('%1$s_%2$s', measureID, trackID);

% Pattern <pm>_<measureID>_<experimentLabel>_<trackID>_<originalTrackID>
EXPERIMENT.pattern.identifier.pm = @(pm, measureID, experimentLabel, trackID, originalTrackID) ...
    sprintf('%1$s_%2$s', pm, EXPERIMENT.pattern.identifier.crowdMeasure(measureID, experimentLabel, trackID, originalTrackID));
% Pattern <pm>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.pmMeasure = @(pm, measureID, trackID) sprintf('%1$s_%2$s_%3$s', pm, measureID, trackID);

% Pattern <rqID>_<type>_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.general =  @(rqID, type, measureID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', rqID, type, measureID, trackID);

% Pattern <rqID>_anova_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.analysis =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'anova', measureID, trackID);

% Pattern <rqID>_obs_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.obs =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'obs', measureID, trackID);

% Pattern <rqID>_tbl_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.tbl =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'tbl', measureID, trackID);

% Pattern <rqID>_sts_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.sts =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'sts', measureID, trackID);

% Pattern <rqID>_me_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.me =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'me', measureID, trackID);

% Pattern <rqID>_ie_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.ie =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'ie', measureID, trackID);

% Pattern <rqID>_mie_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mie =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mie', measureID, trackID);

% Pattern <rqID>_mc_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mc =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'mc', measureID, trackID);

% Pattern <rqID>_soa_<measureID>_<trackID>
EXPERIMENT.pattern.identifier.anova.soa =  @(rqID, measureID, trackID) EXPERIMENT.pattern.identifier.anova.general(rqID, 'soa', measureID, trackID);

% Pattern <rqID>_rep_<trackID>
EXPERIMENT.pattern.identifier.anova.rep =  @(rqID, trackID) sprintf('%1$s_%2$s_%3$s', rqID, 'rep', trackID);



%% Configuration for measures

% The list of measures under experimentation
EXPERIMENT.measure.list = {'ndcg','err','grbp','gap','erap','errbp','ap','apH','rbp','rbpH', 'bpref', 'bprefH', 'infAP', 'infAPH'};
%EXPERIMENT.measure.list = {'bpref', 'bprefH', 'infAP', 'infAPH'};
%EXPERIMENT.measure.list = {'infAP', 'infAPH'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.mapToBinaryRelevance = 'lenient';
EXPERIMENT.measure.ap.compute = @(pool, runSet, shortNameSuffix) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix,'MapToBinaryRelevance',EXPERIMENT.measure.ap.mapToBinaryRelevance);

% Configuration for Hard AP
EXPERIMENT.measure.apH.id = 'apH';
EXPERIMENT.measure.apH.acronym = 'HardAP';
EXPERIMENT.measure.apH.name = 'Average Precision';
EXPERIMENT.measure.apH.mapToBinaryRelevance = 'hard';
EXPERIMENT.measure.apH.compute = @(pool, runSet, shortNameSuffix) averagePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix,'MapToBinaryRelevance',EXPERIMENT.measure.apH.mapToBinaryRelevance);

% Configuration for RBP
EXPERIMENT.measure.rbp.id = 'rbp';
EXPERIMENT.measure.rbp.acronym = 'RBP';
EXPERIMENT.measure.rbp.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp.persistence = 0.8;
EXPERIMENT.measure.rbp.mapToBinaryRelevance = 'lenient';
EXPERIMENT.measure.rbp.compute = @(pool, runSet, shortNameSuffix) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbp.persistence,'MapToBinaryRelevance', EXPERIMENT.measure.rbp.mapToBinaryRelevance);

% Configuration for Hard RBP
EXPERIMENT.measure.rbpH.id = 'rbpH';
EXPERIMENT.measure.rbpH.acronym = 'HardRBP';
EXPERIMENT.measure.rbpH.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbpH.persistence = 0.8;
EXPERIMENT.measure.rbpH.mapToBinaryRelevance = 'hard';
EXPERIMENT.measure.rbpH.compute = @(pool, runSet, shortNameSuffix) rankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbpH.persistence,'MapToBinaryRelevance', EXPERIMENT.measure.rbpH.mapToBinaryRelevance);

% Configuration for graded RBP
EXPERIMENT.measure.grbp.id = 'grbp';
EXPERIMENT.measure.grbp.acronym = 'gRBP';     
EXPERIMENT.measure.grbp.name = 'Graded Rank-biased Precision';
EXPERIMENT.measure.grbp.persistence = 0.8;
EXPERIMENT.measure.grbp.compute = @(pool, runSet, shortNameSuffix) gradedRankBiasedPrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'Persistence', EXPERIMENT.measure.rbp.persistence);

% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain at Last Retrieved Document';
EXPERIMENT.measure.ndcg.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.ndcg.logBase = 10;
EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet, shortNameSuffix) discountedCumulatedGain(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.ndcg.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for ERR
EXPERIMENT.measure.err.id = 'err';
EXPERIMENT.measure.err.acronym = 'ERR';
EXPERIMENT.measure.err.name = 'Expected Reciprocal Rank at Last Retrieved Document';
EXPERIMENT.measure.err.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.err.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.err.compute = @(pool, runSet, shortNameSuffix) expectedReciprocalRank(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'CutOffs', EXPERIMENT.measure.err.cutoffs, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.err.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.err.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for bpref
EXPERIMENT.measure.bpref.id = 'bpref';
EXPERIMENT.measure.bpref.acronym = 'bpref';
EXPERIMENT.measure.bpref.name = 'Binary Preference';
EXPERIMENT.measure.bpref.mapToBinaryRelevance = 'lenient';
EXPERIMENT.measure.bpref.compute = @(pool, runSet, shortNameSuffix) binaryPreference(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'MapToBinaryRelevance',EXPERIMENT.measure.bpref.mapToBinaryRelevance);

% Configuration for bpref hard
EXPERIMENT.measure.bprefH.id = 'bprefH';
EXPERIMENT.measure.bprefH.acronym = 'bprefH';
EXPERIMENT.measure.bprefH.name = 'Binary Preference Hard';
EXPERIMENT.measure.bprefH.mapToBinaryRelevance = 'hard';
EXPERIMENT.measure.bprefH.compute = @(pool, runSet, shortNameSuffix) binaryPreference(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'MapToBinaryRelevance',EXPERIMENT.measure.bprefH.mapToBinaryRelevance);

% Configuration for infAP
EXPERIMENT.measure.infAP.id = 'infAP';
EXPERIMENT.measure.infAP.acronym = 'infAP';
EXPERIMENT.measure.infAP.name = 'Inferred Average Precision';
EXPERIMENT.measure.infAP.mapToBinaryRelevance = 'lenient';
EXPERIMENT.measure.infAP.compute = @(pool, runSet, shortNameSuffix) inferredAveragePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'MapToBinaryRelevance',EXPERIMENT.measure.infAP.mapToBinaryRelevance);

% Configuration for infAP hard
EXPERIMENT.measure.infAPH.id = 'infAPH';
EXPERIMENT.measure.infAPH.acronym = 'infAPH';
EXPERIMENT.measure.infAPH.name = 'Inferred Average Precision Hard';
EXPERIMENT.measure.infAPH.mapToBinaryRelevance = 'hard';
EXPERIMENT.measure.infAPH.compute = @(pool, runSet, shortNameSuffix) inferredAveragePrecision(pool, runSet, 'ShortNameSuffix', shortNameSuffix, 'MapToBinaryRelevance',EXPERIMENT.measure.infAPH.mapToBinaryRelevance);


% Configuration for GAP
EXPERIMENT.measure.gap.id = 'gap';
EXPERIMENT.measure.gap.acronym = 'GAP';
EXPERIMENT.measure.gap.name = 'Graded Average Precision';
EXPERIMENT.measure.gap.probability.l0 = {[1.0]};
EXPERIMENT.measure.gap.probability.l1 = {[1.0,0.0],[0.2,0.8],[0.5,0.5],[0.7,0.3],[0.0,1.0]};
EXPERIMENT.measure.gap.probability.l2 = {[0.2 0.4 0.4],[0.4 0.4 0.2],[0.1 0.4 0.5]};
EXPERIMENT.measure.gap.compute = @(pool, runSet, shortNameSuffix, ThresholdProb) gradedAveragePrecision(pool, runSet, 'RelevanceThresholdProbability', ThresholdProb, 'ShortNameSuffix', shortNameSuffix);

% Configuration for mRAP
EXPERIMENT.measure.erap.id = 'erap';
EXPERIMENT.measure.erap.acronym = 'eRAP';
EXPERIMENT.measure.erap.name = 'expected Random Average Precision';
EXPERIMENT.measure.erap.probability.l0 = {[0.05,0.95],[0.1,0.9],[0.15,0.85],[0.2,0.8],[0.15,0.95],[0.0,1.0]};
EXPERIMENT.measure.erap.probability.l1 = {[0.00,0.00,1.00],[0.00,0.20,1.00],[0.05,0.20,0.95],[0.10,0.20,0.90],[0.00,0.50,1.00],[0.05,0.50,0.95],[0.10,0.50,0.90],[0.00,0.70,1.00],[0.05,0.7,0.95],[0.10,0.70,0.90],[0.00,1.00,1.00]};
EXPERIMENT.measure.erap.probability.l2 = {[0.0 0.2 0.6 1.0],[0.05 0.2 0.6 0.95],[0.0 0.4 0.8 1.0],[0.05 0.4 0.8 0.95],[0.0 0.1 0.5 1.0],[0.05 0.1 0.5 0.95]};
EXPERIMENT.measure.erap.compute = @(pool, runSet, shortNameSuffix, Prob) expectedRandomAveragePrecision(pool, runSet, 'RelevanceProbability', Prob, 'ShortNameSuffix', shortNameSuffix);

% Configuration for mRRBP
EXPERIMENT.measure.errbp.id = 'errbp';
EXPERIMENT.measure.errbp.acronym = 'eRRBP';
EXPERIMENT.measure.errbp.name = 'expected Random Rank-biased Precision';
EXPERIMENT.measure.errbp.persistence = 0.8;
EXPERIMENT.measure.errbp.probability.l0 = EXPERIMENT.measure.erap.probability.l0;
EXPERIMENT.measure.errbp.probability.l1 = EXPERIMENT.measure.erap.probability.l1;
EXPERIMENT.measure.errbp.probability.l2 = EXPERIMENT.measure.erap.probability.l2;
EXPERIMENT.measure.errbp.compute = @(pool, runSet, shortNameSuffix, Prob) expectedRandomRankBiasedPrecision(pool, runSet, 'RelevanceProbability', Prob, 'ShortNameSuffix', shortNameSuffix, 'Persistence' ,EXPERIMENT.measure.errbp.persistence);

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for correlations

% The list of correlation measures under experimentation
EXPERIMENT.correlation.list = {'tau', 'tauAP'};
EXPERIMENT.correlation.number = length(EXPERIMENT.correlation.list);

% Configuration for Kendall's tau correlation
EXPERIMENT.correlation.tau.id = 'tau';
EXPERIMENT.correlation.tau.symbol.tex = '\tau';
EXPERIMENT.correlation.tau.symbol.latex = '$\tau$';
EXPERIMENT.correlation.tau.label.tex = 'Kendall''s \tau Correlation';
EXPERIMENT.correlation.tau.label.latex = 'Kendall''s $\tau$ Correlation';
EXPERIMENT.correlation.tau.name = 'Kendall''s tau correlation';
EXPERIMENT.correlation.tau.color.main = rgb('RoyalBlue');
EXPERIMENT.correlation.tau.color.alternate = rgb('RoyalBlue');
EXPERIMENT.correlation.tau.compute = @(data) corr(data, 'type', 'Kendall');

% Configuration for AP correlation without handling ties
EXPERIMENT.correlation.tauAP.id = 'tauAP';
EXPERIMENT.correlation.tauAP.symbol.tex = '\tau_{AP}';
EXPERIMENT.correlation.tauAP.symbol.latex = '$\tau_{AP}$';
EXPERIMENT.correlation.tauAP.label.tex = 'AP Correlation \tau_{AP}';
EXPERIMENT.correlation.tauAP.label.latex = 'AP Correlation $\tau_{AP}$';
EXPERIMENT.correlation.tauAP.name = 'AP correlation';
EXPERIMENT.correlation.tauAP.color.main = rgb('ForestGreen');
EXPERIMENT.correlation.tauAP.color.alternate = rgb('ForestGreen');
EXPERIMENT.correlation.tauAP.compute = @(data) apCorr(data, 'Symmetric', true, 'Ties', false);


%% Configuration for the kuples to be used

% Number of pools/assessors to be merged
EXPERIMENT.kuples.sizes = 2:30;

EXPERIMENT.kuples.labelNames = strtrim(cellstr(num2str(EXPERIMENT.kuples.sizes(:), 'k = %d')));

% The total number of merges to performa
EXPERIMENT.kuples.number = length(EXPERIMENT.kuples.sizes);

% Maximum number of k-uples to be sampled
EXPERIMENT.kuples.samples = 1000;

%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';

EXPERIMENT.analysis.smallEffect.threshold = 0.06;
EXPERIMENT.analysis.smallEffect.color = 'verylightblue';

EXPERIMENT.analysis.mediumEffect.threshold = 0.14;
EXPERIMENT.analysis.mediumEffect.color = 'lightblue';

EXPERIMENT.analysis.largeEffect.color = 'blue';

% The confidence degree for computing the confidence interval
EXPERIMENT.analysis.ciAlpha = 0.05;

% The number of samples for ties in AP correlation
EXPERIMENT.analysis.apcorrTiesSamples = 100;

% The bandwidth for KDE
EXPERIMENT.analysis.kdeBandwidth = 0.015;

% The bins for kernel density estimation (exceed the range [0 1] to
% compensate for the bandwidth of KSD
EXPERIMENT.analysis.kdeBins = -0.1:0.01:1.1;

% The number of random pools to be generated
EXPERIMENT.analysis.rndPoolsSamples = 1000;


% Commands to be executed to analyse AWARE measures
EXPERIMENT.command.analyse = @(goldMeasure, awareMeasure) analyseKuples(EXPERIMENT.analysis.ciAlpha, EXPERIMENT.analysis.apcorrTiesSamples, goldMeasure, awareMeasure);

