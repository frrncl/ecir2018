%% plot_rank_swaps
%
% Plots the swaps comparing the ranking sistems of two evaluation measure

%% Synopsis
%
%   [] = plot_rank_swaps(measure1, measure2, trackID)
%
% *Parameters*
% * *|measure1|*,*|measure1|* - the identifier of the measure to be used.
% * *|trackID|* - the identifier of the track to process.
%
% *Returns*
%
% Nothing.

%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = plot_rank_swaps(meas1, meas2, trackID)

    % check the number of input parameters
    narginchk(3, inf);

    % load common parameters
    common_parameters

    % check that meas1 and meas2 are a non-empty strings
    validateattributes(meas1, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'meas1');
    validateattributes(meas2, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'meas2');

    % remove useless white spaces, if any, and ensure it is a char row
    meas1 = char(strtrim(meas1));
    meas1 = meas1(:).';
    meas2 = char(strtrim(meas2));
    meas2 = meas2(:).';

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)

        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    plotTitle = char(strtrim(EXPERIMENT.track.(trackID).name));
    plotTitle = plotTitle(:).';
    plotTitle = strrep(plotTitle, '_', '\_');
    
    % load the two measures values among systems and topics
    serload(EXPERIMENT.pattern.file.measure(trackID, [meas1 '_' trackID]),{[meas1 '_' trackID],'Meas1'});
	serload(EXPERIMENT.pattern.file.measure(trackID, [meas2 '_' trackID]),{[meas2 '_' trackID],'Meas2'});

    % compute the mean over topics
    first=mean(Meas1{:,:});
    second=mean(Meas2{:,:});

    nSystems = length(first);

    colors = parula(nSystems);
    colors = colors(end:-1:1, :);
    
    % sort the two rankings
    [~, rankM1] = sort(first, 'ascend');
    [~, rankM2] = sort(second, 'ascend');

    % set the labels
    label = strrep(Meas1.Properties.VariableNames,'_','\_');
    labelLeft = label(rankM1);
    labelRight = label(rankM2);
            
    xTickLabel = cell(1, 2);
    xTickLabel{1} = strrep(Meas1.Properties.UserData.shortName(1:end-4),'_','\_');
    xTickLabel{2} = strrep(Meas2.Properties.UserData.shortName(1:end-4),'_','\_');
    
    data = 1:nSystems;
    data = data .';
    [~, idx] = ismember(rankM1(:), rankM2(:));
    data = [data idx];
    
    currentFigure = figure('Visible', 'off');

    ax = axes('Parent', currentFigure);
    ax1 = axes('Parent', currentFigure);
    ax.TickLabelInterpreter = 'tex';
    ax.FontSize = 48;
    ax.Position = [0.2500 0.1100 0.5000 0.8150];

    ax.XTick = [];

    ax.XLabel.Interpreter = 'tex';
    %title({plotTitle,' '});  
    
    % set the left axis
    ax.YAxisLocation = 'left';
    ax.YLim = [1 nSystems];
    ax.YTick = 1:nSystems;
    ax.YTickLabel = [];
    ax.YLabel.String = ['Systems sorted by ' xTickLabel{1}];
    ax.YLabel.Interpreter = 'tex';
    % ax.TickDir = 'out';
    
    hold on;

    ax1.XTick = 1:2;
    ax1.FontSize = 48;
    ax1.XTickLabel = []; %xTickLabel;
    
    % set the right axis
    ax1.YAxisLocation = 'right';
    ax1.YLim = [1 nSystems];
    ax1.YTick = 1:nSystems;
    ax1.YTickLabel = [];
    ax1.YLabel.String = ['Systems sorted by ' xTickLabel{2}];
    ax1.YLabel.Interpreter = 'tex';
    %ax1.TickDir = 'out';
    ax1.Position = [0.2500 0.1100 0.5000 0.8150];

    % for each measure
    for t = 1:2
        plot([t t], [1 nSystems], 'k', 'LineWidth', 0.5)
    end;
      
    % for each label
    for l = 1:nSystems

        plot(1:2, data(l, :), '-', ...
            'Color', colors(l, :), 'LineWidth', 6, ...
            'Marker', 'o', 'MarkerSize', 6, ...
            'MarkerFaceColor', colors(l, :), ...
            'MarkerEdgeColor',  colors(l, :));
    end;
    
    currentFigure.PaperPositionMode = 'auto';
    currentFigure.PaperUnits = 'centimeters';
    currentFigure.PaperSize = [82 52];
    currentFigure.PaperPosition = [17 1 50 50];

    figureID = EXPERIMENT.pattern.identifier.figure.general([meas1 '_' meas2], trackID);

    print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

    close(currentFigure)

    diary off;
end