dpStats = true;
deltaStats = true;

common_parameters;


% start of overall computations
startComputation = tic;

fprintf('\n\n######## Plotting rankings swaps between measures, Self Kendall correlations at pool samples and Discriminative powers on different collection (%s) ########\n\n', ...
    EXPERIMENT.label.paper);

fprintf('+ Settings\n');
fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));

%trackList = {'T13','T14','T15','T21'};%
trackList = {'T14','T21'};%
pListGapT1_ = {[0.2 0.8]; [0.5 0.5]; [0.7 0.3]};
pListRapT1_ = {[0.0 0.2 1.0] [0.05 0.2 0.95]; [0.0 0.5 1.0] [0.05 0.5 0.95]; [0.0 0.7 1.0] [0.05 0.7 0.95]};
pListGapT21 = {[0.2 0.4 0.4];[0.4 0.4 0.2];[0.1 0.4 0.5]};
pListRapT21 = {[0.0 0.2 0.6 1.0],[0.05 0.2 0.6 0.95];[0.0 0.4 0.8 1.0],[0.05 0.4 0.8 0.95];[0.0 0.1 0.5 1.0],[0.05 0.1 0.5 0.95]};

reference = {'nDCG','ERR', 'bpref', 'infAP'};

for i = 1:length(trackList)
   trackID = trackList{i};
   figureID = EXPERIMENT.pattern.identifier.figure.general('SelfKendall_050_05', trackID);
   outputPath = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.figure, trackID, filesep);
   
   fprintf('+ Working on collection %s\n', ...
    trackID);
   
   if ~strcmp(trackID,'T21')
      pListGap = pListGapT1_; 
      pListRap = pListRapT1_;
   else
      pListGap = pListGapT21; 
      pListRap = pListRapT21;
   end   
   
   
   
   for k = 1:length(pListGap)
       
       fileNameSuffix = num2str(pListGap{k,1}*100, '_%03.0f');
       
       Gap = EXPERIMENT.pattern.identifier.pid('GAP',pListGap{k,1});
       Rap000 = EXPERIMENT.pattern.identifier.pid('eRAP',pListRap{k,1});
       Rap005 = EXPERIMENT.pattern.identifier.pid('eRAP',pListRap{k,2});
       Rrbp000 = EXPERIMENT.pattern.identifier.pid('eRRBP',pListRap{k,1});
       Rrbp005 = EXPERIMENT.pattern.identifier.pid('eRRBP',pListRap{k,2});
    
       observed = {Gap, Rap000, Rap005, 'gRBP', Rrbp000, Rrbp005};
       selfPlot = {Rap005, Rrbp005};
         
        plot_rank_swaps(EXPERIMENT.pattern.identifier.pid('gap',pListGap{k,1}),EXPERIMENT.pattern.identifier.pid('erap',pListRap{k,1}),trackID)
        plot_rank_swaps(EXPERIMENT.pattern.identifier.pid('gap',pListGap{k,1}),EXPERIMENT.pattern.identifier.pid('erap',pListRap{k,2}),trackID)
        plot_rank_swaps('grbp',EXPERIMENT.pattern.identifier.pid('errbp',pListRap{k,1}),trackID)
        plot_rank_swaps('grbp',EXPERIMENT.pattern.identifier.pid('errbp',pListRap{k,2}),trackID)
     
        plotSelfKendallAtPoolSamples('Observed', observed,'Reference', reference,...
            'TrackID', trackID, 'OutputPath', outputPath, 'FileNameSuffix', fileNameSuffix);
        
        plotDiscriminativePowerAtPoolSamples('Observed', observed,'Reference', reference,...
             'SelfPlot',selfPlot,...
            'TrackID', trackID, 'OutputPath', outputPath, 'FileNameSuffix', fileNameSuffix,...
            'DpStats', dpStats, 'DeltaStats', deltaStats);
     
       end
   
end % ciclo sui vari treck

fprintf('\n\n######## Total elapsed time for plotting main effects for evaluation measures on different collections(%s):  \n %s ########\n\n', ...
    EXPERIMENT.label.paper, ...
    elapsedToHourMinutesSeconds(toc(startComputation)));

clear all
