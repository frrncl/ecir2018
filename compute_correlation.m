%% compute_correlation
% 
% Computes correlation among measures for the given track and saves them 
% to a |.mat| file.
%
%% Synopsis
%
%   [] = compute_correlation(trackID, systemsID, topicsID, remove1Q, varargin)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to process.
% * *|systemsID|* - the identifier of the systems sample to be used.
% * *|topicsID|* - the identifier of the topics sample to be used.
% * *|remove1Q|* - a boolean indicating whether the systems in the first
% quartile of AP performances have to be removed or not.
% * *|correlationID|* - a list of identifiers of correlations to be
% computed.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = compute_correlation(trackID, pListID, varargin)
   
    % check the number of input arguments
    narginchk(2, inf);

    % setup common parameters
    common_parameters;
        
    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
     % check that pListID is a non-empty string
    validateattributes(pListID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'pListID');

    if iscell(pListID)
        % check that pListID is a cell array of strings with one element
        assert(iscellstr(pListID) && numel(pListID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected pListID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    pListID = char(strtrim(pListID));
    pListID = pListID(:).';
    
     % check that pListID assumes a valid value
    validatestring(pListID, ...
        fieldnames(EXPERIMENT.measure.gap.probability), '', 'pListID');
    
    for k = 1:length(varargin)

        correlationID = varargin{k};
        
        % check that correlationID is a non-empty string
        validateattributes(correlationID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'correlationID');

         if iscell(correlationID)
            % check that correlationID is a cell array of strings with one element
            assert(iscellstr(correlationID) && numel(correlationID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected correlationID to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        correlationID = char(strtrim(correlationID));
        correlationID = correlationID(:).';
    
        % check that correlationID assumes a valid value
        validatestring(correlationID, ...
            EXPERIMENT.correlation.list, '', 'correlationID');
        
        varargin{k} = correlationID;
    end;
    
    % start of overall computations
    startComputation = tic;

    fprintf('\n\n######## Computing correlation(s)  on collection %s (%s) ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
 
    
    measureIDList = [];
    measureLabel = [];
    
    for m = 1: EXPERIMENT.measure.number
        
        mid = EXPERIMENT.measure.list{m};
        
        if isfield(EXPERIMENT.measure.(mid),'probability')
            
            pList = EXPERIMENT.measure.(mid).probability.(pListID);
            
            for p = 1 : length(pList)
                
                pid = pList{p};
                
                measureIDList = [measureIDList {EXPERIMENT.pattern.identifier.measure(EXPERIMENT.pattern.identifier.pid(mid, pid), trackID)}];                
                
                measureLabel = [measureLabel {EXPERIMENT.pattern.identifier.pid(EXPERIMENT.measure.getAcronym(m), pid)}];
                
            end
            
        else
            measureIDList = [measureIDList {EXPERIMENT.pattern.identifier.measure(mid, trackID)}];
            
            measureLabel = [measureLabel {EXPERIMENT.measure.getAcronym(m)}];
            
        end;        
    end    
    
    measureNumber = length(measureIDList);
    
        
    fprintf('+ Loading measures\n');
        
    measures = [];
    
    % for each measure
    for m = 1:measureNumber
        
        fprintf('+ loading %s\n', measureIDList{m});
                
        % load the measure in the tmp variable
        serload2(EXPERIMENT.pattern.file.measure(trackID, measureIDList{m}), ...
        'WorkspaceVarNames', {'measure'}, ...
        'FileVarNames', {measureIDList{m}});
                
        measures = [measures mean(measure{:, :}).'];
        
        clear('measure');
        
    end;
      
    
    % Labels of the correlation pairs
    combinations = combnk(1:measureNumber, 2);
    
    % Serve per mettere i Label nell'ordine giusto rispetto alle
    % correlazioni
    if combinations(1,1)~=1
        combinations = combinations(end:-1:1,:);
    end
           
    % indexes of the upper triangle
    idx = logical(tril(ones(measureNumber), -1));

    for k = 1:length(varargin)
    
    	correlation.labels.list = measureLabel(combinations);
   		correlation.labels.first = correlation.labels.list(:, 1);
    	correlation.labels.second = correlation.labels.list(:, 2);
    	correlation.labels.list(:, 3) = correlation.labels.list(:, 2);
    	correlation.labels.list(:, 2) = {'_'};
    	correlation.labels.list = strcat(correlation.labels.list(:, 1), correlation.labels.list(:, 2), correlation.labels.list(:, 3));
    	correlation.labels.number = length(correlation.labels.list);   
        
        start = tic;
        
        correlationID = varargin{k};
        
        fprintf('+ Computing the %s correlation\n', correlationID);
        
        % identifier of the computed correlation
        correlation.id = correlationID;
        
        % rows are subjects, columns are correlation values for each pair
        % of measures
        correlation.values = EXPERIMENT.correlation.(correlationID).compute(measures);
        correlation.table = array2table(correlation.values);
        correlation.table.Properties.VariableNames = measureLabel;
        correlation.table.Properties.RowNames = measureLabel.';
        
        
        correlation.values = correlation.values(idx).';
        
        corrID = EXPERIMENT.pattern.identifier.correlation(correlationID, pListID, trackID);
        
        sersave2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
                    'WorkspaceVarNames', {'correlation'}, ...
                    'FileVarNames', {corrID});
        
        clear('correlation', corrID);
                    
        fprintf('  - elapsed time %s\n', elapsedToHourMinutesSeconds(toc(start)));
        
    end;
       
    fprintf('\n\n######## Total elapsed time for computing correlation(s) on collection %s (%s): %s ########\n\n', ...
            EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end
