
common_parameters;

%trackList = {'T13','T14','T15','T21'};
trackList = {'T14', 'T21'};
tracks = length(trackList);

corrList = {'tau','tauAP'};

pList = {'l1','l2'};

pListGapT1_ = {[0.2 0.8]; [0.5 0.5]; [0.7 0.3]};
pListRapT1_ = {[0.0 0.2 1.0] [0.05 0.2 0.95]; [0.0 0.5 1.0] [0.05 0.5 0.95]; [0.0 0.7 1.0] [0.05 0.7 0.95]};
pListGapT21 = {[0.2 0.4 0.4];[0.4 0.4 0.2];[0.1 0.4 0.5]};
pListRapT21 = {[0.0 0.2 0.6 1.0],[0.05 0.2 0.6 0.95];[0.0 0.4 0.8 1.0],[0.05 0.4 0.8 0.95];[0.0 0.1 0.5 1.0],[0.05 0.1 0.5 0.95]};

if (tracks > 1)
    tracks_list = strjoin(trackList, ' ');
    corr_list = strjoin(corrList, '_');
else
    tracks_list = trackList{1};
end

fprintf('\n\n######## Report the correlations (tau and tauAP) among measures for each collection on %s (%s) ########\n\n', ...
    tracks_list, EXPERIMENT.label.paper);

fprintf('+ Settings\n');
fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
fprintf('  - tracks %s\n', tracks_list);

for i = 1:tracks
    
    trackID = trackList{i};
    
    if ~strcmp(trackID,'T21')
        pListID = 'l1';
    else
        pListID = 'l2';
    end
    
    if ~strcmp(trackID,'T21')
        pListGap = pListGapT1_;
        pListRap = pListRapT1_;
    else
        pListGap = pListGapT21;
        pListRap = pListRapT21;
    end
    
    % setup common parameters
    correlations = {};
    labels = {};
    
    fprintf('+ Loading data\n');
    
    for c = 1:length(corrList)
        
        corrID = EXPERIMENT.pattern.identifier.correlation(corrList{c}, pListID, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
            'WorkspaceVarNames', {'corrVar'}, ...
            'FileVarNames', {corrID});
        
        correlations = [correlations; {corrVar.values}];
        
        labels = [labels; {corrVar.labels.list}];
        
        % free space
        clear('corrVar');
        
    end; % correlation
    
    
    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, sprintf('%s_%s', corr_list, trackID)), 'w');
    
    
    fprintf(fid, '\\documentclass[11pt]{article} \n\n');
    
    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on the Measure tau and tauAP correlations for collection %s}\n\n', trackID);
    
    fprintf(fid, '\\maketitle\n\n');
    
    for k = 1:length(pListGap)
        
        fileNameSuffix = num2str(pListGap{k,1}*100, '_%03.0f');
        
        Gap = EXPERIMENT.pattern.identifier.pid('GAP',pListGap{k,1});
        Rap000 = EXPERIMENT.pattern.identifier.pid('eRAP',pListRap{k,1});
        Rap005 = EXPERIMENT.pattern.identifier.pid('eRAP',pListRap{k,2});
        Rrbp000 = EXPERIMENT.pattern.identifier.pid('eRRBP',pListRap{k,1});
        Rrbp005 = EXPERIMENT.pattern.identifier.pid('eRRBP',pListRap{k,2});
        
        measureLabel = {'nDCG','ERR', 'bpref', 'infAP', 'GAP', 'eRAP$_1$', 'eRAP$_2$', 'gRBP', 'eRRBP$_1$', 'eRRBP$_2$'};
        
        measureID = {'nDCG','ERR', 'bpref', 'infAP', Gap, Rap000, Rap005, 'gRBP', Rrbp000, Rrbp005};
        
        %fprintf(fid, '\\begin{landscape}  \n');
        fprintf(fid, '\\begin{table}[p] \n');
        % fprintf(fid, '\\tiny \n');
        fprintf(fid, '\\centering \n');
        %fprintf(fid, '\\hspace*{-6.5em} \n');
        
        fprintf(fid, '\\begin{tabular}{|ll|*{%d}{r|}} \n', length(measureID));
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '& ');
        for m = 1:length(measureID)
            fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{%s}} ', measureLabel{m});
        end; % measure
        
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        
        % for each correlation pair
        for i = 1:length(measureID)
            
            % tau
            fprintf(fid, '\\multirow{2}{*}{\\textbf{%s}} & $\\tau$ ', measureLabel{i});
            
            for j = 1:length(measureID)
                
                if (j < i)
                    fprintf(fid, '& -- ');
                elseif (j == i)
                    fprintf(fid, '& %.4f ', 1.000);
                else
                    % the index of this pair in the vectors
                    idx = ismember(labels{1,:}, sprintf('%s_%s', measureID{i}, measureID{j}));
                    if isempty(find(idx))
                        idx = ismember(labels{1,:}, sprintf('%s_%s',  measureID{j},measureID{i}));
                    end
                    %
                    fprintf(fid, '& %.4f ', correlations{1,1}(idx));
                end;
            end;
            fprintf(fid, '\\\\ \n');
            
            % tauAP
            fprintf(fid, ' & $\\tau_{AP}$ ');
            
            
            for j = 1:length(measureID)
                if (j < i)
                    fprintf(fid, '& -- ');
                elseif (j == i)
                    fprintf(fid, '& %.4f ', 1.000);
                else
                    % the index of this pair in the vectors
                    idx = ismember(labels{2,:}, sprintf('%s_%s', measureID{i}, measureID{j}));
                    if isempty(find(idx))
                        idx = ismember(labels{2,:}, sprintf('%s_%s',  measureID{j},measureID{i}));
                    end
                    fprintf(fid, '& %.4f ', correlations{2,1}(idx));
                end;
            end;
            fprintf(fid, '\\\\ \n');
            
            fprintf(fid, '\\hline \n');
        end;
        
        fprintf(fid, '\\end{tabular} \n');
        
        fprintf(fid, '\\caption{Measure Pair main effects of the $\\tau$ and $\\tau_{AP}$ correlation coefficients. ');
        
        if ~strcmp(trackID,'T21')
            fprintf(fid, 'eRAP$_1$ and eRRBP$_1$ have probability values equal to $[%0.02f,%0.02f,%0.02f]$, ', pListRap{k,1}(1), pListRap{k,1}(2), pListRap{k,1}(3));
        else
            fprintf(fid, 'eRAP$_1$ and eRRBP$_1$ have probability values equal to $[%0.02f,%0.02f,%0.02f,%0.02f]$, ', pListRap{k,1}(1), pListRap{k,1}(2), pListRap{k,1}(3), pListRap{k,1}(4));
        end
        if ~strcmp(trackID,'T21')
            fprintf(fid, 'eRAP$_2$ and eRRBP$_2$ equal to $[%0.02f,%0.02f,%0.02f],$ ', pListRap{k,2}(1), pListRap{k,2}(2), pListRap{k,2}(3));
        else
            fprintf(fid, 'eRAP$_2$ and eRRBP$_2$ equal to $[%0.02f,%0.02f,%0.02f,%0.02f],$ ', pListRap{k,2}(1), pListRap{k,2}(2), pListRap{k,2}(3),pListRap{k,2}(4));
        end
        if ~strcmp(trackID,'T21')
            fprintf(fid, 'while GAP has the threshold  probabilities equal to $[%0.02f,%0.02f].$\n}', pListGap{k,1}(1), pListGap{k,1}(2));
        else
            fprintf(fid, 'while GAP has the threshold  probabilities equal to $[%0.02f,%0.02f,%0.02f].$\n}', pListGap{k,1}(1), pListGap{k,1}(2), pListGap{k,1}(3));
        end
        fprintf(fid, '\\end{table} \n\n');
        
    end
    
    fprintf(fid, '\\end{document} \n\n');
    
    
    fclose(fid);
    
    diary off;
end
