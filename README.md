Matlab source code for the experiments reported in the paper:

Ferrante, M., Ferro, N., and Pontarollo, S. (2018). Modelling Randomness in Relevance Judgments and Evaluation Measures. In Pasi, G., Piwowarski, B., Azzopardi, L., and Hanbury, A., editors, *Advances in Information Retrieval. Proc. 40th European Conference on IR Research (ECIR 2018)*. Lecture Notes in Computer Science (LNCS), Springer, Heidelberg, Germany.
