dpStats = true;
deltaStats = true;

common_parameters;


% start of overall computations
startComputation = tic;

fprintf('\n\n######## Plotting rankings swaps between measures, Self Kendall correlations at pool samples and Discriminative powers on different collection (%s) ########\n\n', ...
    EXPERIMENT.label.paper);

fprintf('+ Settings\n');
fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));

trackList = {'T13','T21'};%
pListGapT1_ = {[0.2 0.8]; [0.5 0.5]; [0.7 0.3]};
pListRapT1_ = {[0.0 0.2 1.0] [0.05 0.2 0.95]; [0.0 0.5 1.0] [0.05 0.5 0.95]; [0.0 0.7 1.0] [0.05 0.7 0.95]};
pListGapT21 = {[0.1 0.4 0.5];[0.2 0.4 0.4];[0.4 0.4 0.2]};
pListRapT21 = {[0.0 0.1 0.5 1.0],[0.05 0.1 0.5 0.95];[0.0 0.2 0.6 1.0],[0.05 0.2 0.6 0.95];[0.0 0.4 0.8 1.0],[0.05 0.4 0.8 0.95]};


for i = 1:length(trackList)
    trackID = trackList{i};
    figureID = EXPERIMENT.pattern.identifier.figure.general('SelfKendall_050_05', trackID);
    outputPath = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.figure, trackID, filesep);
    
    fprintf('+ Working on collection %s\n', ...
        trackID);
    
    if ~strcmp(trackID,'T21')
        pListGap = pListGapT1_;
        pListRap = pListRapT1_;
    else
        pListGap = pListGapT21;
        pListRap = pListRapT21;
    end
    
    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, sprintf('DP_DELTA_%s', trackID)), 'w');
    
    
    fprintf(fid, '\\documentclass[11pt]{article} \n\n');
    
    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on the Measure tau and tauAP correlations for collection %s, Trec %s}\n\n', trackID, trackList{i});
    
    fprintf(fid, '\\maketitle\n\n');
    
    for k = 1:length(pListGap)
        
        fileNameSuffix = num2str(pListGap{k,1}*100, '_%03.0f');
        
        Gap = EXPERIMENT.pattern.identifier.pid('GAP',pListGap{k,1});
        Rap000 = EXPERIMENT.pattern.identifier.pid('eRAP',pListRap{k,1});
        Rap005 = EXPERIMENT.pattern.identifier.pid('eRAP',pListRap{k,2});
        Rrbp000 = EXPERIMENT.pattern.identifier.pid('eRRBP',pListRap{k,1});
        Rrbp005 = EXPERIMENT.pattern.identifier.pid('eRRBP',pListRap{k,2});
        
        measures = {Gap, Rap000, Rap005, 'gRBP', Rrbp000, Rrbp005,'nDCG','ERR'};
        measureLabel = {'GAP', 'eRAP$_1$', 'eRAP$_2$', 'gRBP', 'eRRBP$_1$', 'eRRBP$_2$','nDCG','ERR'};
        
        dpList = cell(length(measures),1);
        deltaList = cell(length(measures),1);
        for m = 1:length(measures)
            corrID = EXPERIMENT.pattern.identifier.downsampledDPower(measures{m}, trackID);
            
            dpID = EXPERIMENT.pattern.identifier.pmMeasure('dp', measures{m}, trackID);
            deltaID = EXPERIMENT.pattern.identifier.pmMeasure('delta', measures{m}, trackID);
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
                'WorkspaceVarNames', {'dp','delta'}, ...
                'FileVarNames', {dpID,deltaID});
            
            dpList{m} = dp;
            deltaList{m} = delta;
            % free space
            clear('dp','delta');
            
        end
        
        
        
        
        %fprintf(fid, '\\begin{landscape}  \n');
        fprintf(fid, '\\begin{table}[p] \n');
        % fprintf(fid, '\\tiny \n');
        fprintf(fid, '\\centering \n');
        %fprintf(fid, '\\hspace*{-6.5em} \n');
        
        
        fprintf(fid, '\\begin{tabular}{|ll|*{%d}{r|}} \n', length(measures));
        
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '& ');
        for m = 1:length(measures)
            fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{%s}} ', measureLabel{m});
        end; % measure
        
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        
        % for each correlation pair
        for w = 1:(length(dpList{1}.Properties.UserData.sampleSize)+1)
            tmp = strrep(dpList{1}.Properties.VariableNames{w}, '_', '\_');
            % tau
            fprintf(fid, '\\multirow{2}{*}{$\\mathbf{%s}$} & DP ', tmp);
            
            for j = 1:length(measures)
                
                fprintf(fid, '& %4.2f ', dpList{j}{1, w}*100);
                
            end;
            fprintf(fid, '\\\\ \n');
            
            % tauAP
            fprintf(fid, ' & Delta');
            
            
            for j = 1:length(measures)
                fprintf(fid, '& %5.2f ', deltaList{j}{1, w});
            end;
            fprintf(fid, '\\\\ \n');
            
            fprintf(fid, '\\hline \n');
        end;
        
        fprintf(fid, '\\end{tabular} \n');
        fprintf(fid, '\\caption{ DS values (i.e. the ratio of the number of times the requested ASL has been achieved over all the possible system pairs)');
        fprintf(fid,'and Delta values (i.e  the estimated difference needed to expect two system being significantly different), ');
        fprintf(fid,'for every downsampling rate');
        
        if ~strcmp(trackID,'T21')
            fprintf(fid, 'eRAP$_1$ and eRRBP$_1$ have probability values equal to $[%0.02f,%0.02f,%0.02f]$, ', pListRap{k,1}(1), pListRap{k,1}(2), pListRap{k,1}(3));
        else
            fprintf(fid, 'eRAP$_1$ and eRRBP$_1$ have probability values equal to $[%0.02f,%0.02f,%0.02f,%0.02f]$, ', pListRap{k,1}(1), pListRap{k,1}(2), pListRap{k,1}(3), pListRap{k,1}(4));
        end
        if ~strcmp(trackID,'T21')
            fprintf(fid, 'eRAP$_2$ and eRRBP$_2$ equal to $[%0.02f,%0.02f,%0.02f],$ ', pListRap{k,2}(1), pListRap{k,2}(2), pListRap{k,2}(3));
        else
            fprintf(fid, 'eRAP$_2$ and eRRBP$_2$ equal to $[%0.02f,%0.02f,%0.02f,%0.02f],$ ', pListRap{k,2}(1), pListRap{k,2}(2), pListRap{k,2}(3),pListRap{k,2}(4));
        end
        if ~strcmp(trackID,'T21')
            fprintf(fid, 'while GAP has the threshold  probabilities equal to $[%0.02f,%0.02f].$\n}', pListGap{k,1}(1), pListGap{k,1}(2));
        else
            fprintf(fid, 'while GAP has the threshold  probabilities equal to $[%0.02f,%0.02f,%0.02f].$\n}', pListGap{k,1}(1), pListGap{k,1}(2), pListGap{k,1}(3));
        end
        
        fprintf(fid, '\\end{table} \n\n');
        
    end
    
    fprintf(fid, '\\end{document} \n\n');
    
    fclose(fid);
    
    diary off;
    
    
    
    
end

fprintf('\n\n######## Total elapsed time for plotting main effects for evaluation measures on different collections(%s):  \n %s ########\n\n', ...
    EXPERIMENT.label.paper, ...
    elapsedToHourMinutesSeconds(toc(startComputation)));

clear all
