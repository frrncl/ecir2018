%% expectedRandomAveragePrecision
%
% Computes the expectation of random average precision (eRAP).

%% Synopsis
%
%   [measuredRunSet, poolStats, runSetStats, inputParams] = expectedRandomAveragePrecision(pool, runSet, Name, Value)
%
% Note that expected random average precision will be 0 when there are no relevant
% documents for a given topic in the pool.
%
% *Parameters*
%
% * *|pool|* - the pool to be used to assess the run(s). It is a table in the
% same format returned by <../io/importPoolFromFileTRECFormat.html
% importPoolFromFileTRECFormat>;
% * *|runSet|* - the run(s) to be assessed. It is a table in the same format
% returned by <../io/importRunFromFileTRECFormat.html
% importRunFromFileTRECFormat> or by <../io/importRunsFromDirectoryTRECFormat.html
% importRunsFromDirectoryTRECFormat>;
%
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the
% argument name and |Value| is the corresponding value. |Name| must appear
% inside single quotes (' '). You can specify several name and value pair
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% * *|RelevanceProbability|* (mandatory) - a numeric vector of
% probabilities that a user sees as relevant the given relevance degree.
% It must have as many elements as the number of relevance degrees.
% * *|ShortNameSuffix|* (optional) - a string providing a suffix which will
% be concatenated to the short name of the measure. It can contain only
% letters, numbers and the underscore. The default is empty.
% * *|NotAssessed|* (optional) - a string indicating how not assessed
% documents, i.e. those in the run but not in the pool, have to be
% processed: |NotRevelant|, the minimum of the relevance degrees of the
% pool is used as |NotRelevant|; |Condensed|, the not assessed documents
% are  removed from the run. If not specified, the default value  is
% |NotRelevant| to mimic the behaviour of trec_eval.
% * *|MapToBinaryRelevance|* (optional) - a string specifying how relevance
% degrees have to be mapped to binary relevance. The following values can
% be used: _|Hard|_ considers only the maximum degree of relevance in the
% pool as |Relevant| and any degree below it as |NotRelevant|; _|Lenient|_
% considers any degree of relevance in the pool above the minimum one as
% |Relevant| and only the minimum one is considered as |NotRelevant|;
% _|RelevanceDegree|_ considers the relevance degrees in the pool stricly
% above the specified one as |Relevant| and all those less than or equal to
% it as |NotRelevant|. In this latter case, if |RelevanceDegree| does not
% correspond to any of the relevance degrees in the pool, an error is
% raised.
% * *|Verbose|* (optional) - a boolean specifying whether additional
% information has to be displayed or not. If not specified, then |false| is
% used as default.
%
% *Returns*
%
% * |measureRunSet|  - a table containing a row for each topic and a column
% for each run named |runName|. Each cell of the table contains a scalar
% representing the expected random average precision. The |UserData| property of
% the table contains a struct  with the  following fields: _|identifier|_ is
% the identifier of the run; _|name|_  is the name of the computed measure,
% i.e. |expectedRandomAveragePrecision|; _|shortName|_ is a short name of the
% computed measure, i.e. |eRAP|; _|pool|_ is the identifier of the pool with
% respect to which the measure has been computed.
% * *|poolStats|* - see description in <assess.html assess>.
% * *|runSetStats|* - see description in <assess.html assess>.
% * *|inputParams|* - a struct summarizing the input parameters passed.

%% Example of use
%
%
%
%% References
%
%
%
%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% <mailto:maistro@dei.unipd.it Maria Maistro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2013-2014 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [measuredRunSet, poolStats, runSetStats, inputParams] = expectedRandomAveragePrecision(pool, runSet, varargin)

    persistent UNSAMPLED_UNJUDGED;

    if isempty(UNSAMPLED_UNJUDGED)

        % New categorical value to be added to the pool to indicate not
        % sampled documents or documents that have been pooled but
        % unjudged. See Yilmaz and Aslam, CIKM 2006
        UNSAMPLED_UNJUDGED = 'U_U';

    end;

    % check that we have the correct number of input arguments.
    narginchk(2, inf);

    % parse the variable inputs
    pnames = {'ShortNameSuffix', 'RelevanceProbability' 'MapToBinaryRelevance' 'NotAssessed' 'Verbose'};
    dflts =  {[]                 []                              []                     'NotRelevant' false};
    
    if verLessThan('matlab', '9.2.0')
        [shortNameSuffix, RelevanceProbability, mapToBinaryRelevance, notAssessed, verbose, supplied] ...
            = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [shortNameSuffix, RelevanceProbability, mapToBinaryRelevance, notAssessed, verbose, supplied] ...
            = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end
       
    % actual parameters to be passed to assess.m, at least 6
    assessInput = cell(1, 8);

    % not assessed documents must be considered as not relevant for
    % precision
    assessInput{1, 1} = 'NotAssessed';
    assessInput{1, 2} = notAssessed;

    if supplied.MapToBinaryRelevance
        % there will be only two relevance degrees after mapping (no matter
        % their names for the following settings in assessInput)
        relevanceDegrees = {'BinaryNotRelevant', 'BinaryRelevant'};
        % the total number of relevance degrees above not relevant
        D = length(relevanceDegrees) - 1;
    else
        % get the relevance degrees in the pools
        relevanceDegrees = categories(pool{:, 1}{1, 1}.RelevanceDegree);
        relevanceDegrees = relevanceDegrees(:).';
    end;
    if strcmpi(relevanceDegrees{1}, UNSAMPLED_UNJUDGED)
        % the total number of relevance degrees above not relevant
        D = length(relevanceDegrees) - 2;
    else
        % the total number of relevance degrees above not relevant
        D = length(relevanceDegrees) - 1;
    end;
    % set default 1-based relevance weights. They are not real weights but
    % they actually represent indexes into the RelevanceProbability
    % vector, for "weights" above the not relevant one (0)
    mapToRelevanceWeights = 0:D;

    % map to relevance weights must be performed to ease subsequent
    % computations
    assessInput{1, 3} = 'MapToRelevanceWeights';
    assessInput{1, 4} = mapToRelevanceWeights;

    % remove unsampled/unjudged documents because they are not appropriate
    % for precision computation
    assessInput{1, 5} = 'RemoveUUFromPool';
    assessInput{1, 6} = true;

    % padding is not needed
    assessInput{1, 7} = 'FixNumberRetrievedDocuments';
    assessInput{1, 8} = [];

    % add the mapping to binary relevance, if needed
    if supplied.MapToBinaryRelevance
        assessInput{1, end+1} = 'MapToBinaryRelevance';
        assessInput{1, end+1} = mapToBinaryRelevance;
    end;

    if supplied.RelevanceProbability

        % it must have as many elements as the number of relevance degrees,
        % excluding the NotRelevant degree and UNSAMPLE_UNJUDGED, if
        % present; moreover it is an ascendent vector with the last entry
        % equal to one
        if strcmpi(relevanceDegrees{1}, UNSAMPLED_UNJUDGED)
            validateattributes(RelevanceProbability, ...
                {'numeric'}, ...
                {'nonempty', 'vector', 'numel', length(relevanceDegrees)-1, '>=', 0, '<=', 1}, '', ...
                'RelevanceProbability');
        else
            validateattributes(RelevanceProbability, ...
                {'numeric'}, ...
                {'nonempty', 'vector', 'numel', length(relevanceDegrees), '>=', 0, '<=', 1}, '', ...
                'RelevanceProbability');
        end;
        
        % ensure it is a row vector
        RelevanceProbability = RelevanceProbability(:).';
    else
        error('MATTERS:IllegalArgument', 'The relevance threshold probabilities are missing.');
    end;


    if supplied.ShortNameSuffix
        if iscell(shortNameSuffix)
            % check that nameSuffix is a cell array of strings with one element
            assert(iscellstr(shortNameSuffix) && numel(shortNameSuffix) == 1, ...
                'MATTERS:IllegalArgument', 'Expected NameSuffix to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        shortNameSuffix = char(strtrim(shortNameSuffix));
        shortNameSuffix = shortNameSuffix(:).';

        % check that the nameSuffix is ok according to the matlab rules
        if ~isempty(regexp(shortNameSuffix, '\W*', 'once'))
            error('MATTERS:IllegalArgument', 'NameSuffix %s is not valid: it can contain only letters, numbers, and the underscore.', ...
                shortNameSuffix);
        end

        % if it starts with an underscore, remove it since il will be
        % appended afterwards
        if strcmp(shortNameSuffix(1), '_')
            shortNameSuffix = shortNameSuffix(2:end);
        end;
    end;

    if supplied.Verbose
        % check that verbose is a non-empty scalar logical value
        validateattributes(verbose, {'logical'}, {'nonempty','scalar'}, '', 'Verbose');
    end;

    if verbose
        fprintf('\n\n----------\n');

        fprintf('Computing expected random graded average precision for run set %s with respect to pool %s: %d run(s) and %d topic(s) to be processed.\n\n', ...
            runSet.Properties.UserData.identifier, pool.Properties.UserData.identifier, width(runSet), height(runSet));
    end;

    [assessedRunSet, poolStats, runSetStats, inputParams] = assess(pool, runSet, 'Verbose', verbose, assessInput{:});

    % the topic currently under processing
    ct = 1;

    % the run currently under processing
    cr = 1;

    % compute the measure topic-by-topic
    measuredRunSet = rowfun(@processTopic, assessedRunSet, 'OutputVariableNames', runSet.Properties.VariableNames, 'OutputFormat', 'table', 'ExtractCellContents', true, 'SeparateInputs', false);
    measuredRunSet.Properties.UserData.identifier = assessedRunSet.Properties.UserData.identifier;
    measuredRunSet.Properties.UserData.pool = pool.Properties.UserData.identifier;

    measuredRunSet.Properties.UserData.name = 'expectedRandomAveragePrecision';
    measuredRunSet.Properties.UserData.shortName = 'eRAP';

    tmp = num2str(RelevanceProbability*100, '%03.0f_');
    tmp = tmp(1:end-1);

    measuredRunSet.Properties.UserData.name = [measuredRunSet.Properties.UserData.name '_' tmp];
    measuredRunSet.Properties.UserData.shortName = [measuredRunSet.Properties.UserData.shortName '_' tmp];

    if ~isempty(shortNameSuffix)
        measuredRunSet.Properties.UserData.shortName = [measuredRunSet.Properties.UserData.shortName ...
            '_' shortNameSuffix];
    end;

    if verbose
        fprintf('Computation of expected random average precision completed.\n');
    end;

%%

% compute the measure for a given topic over all the runs
    function [varargout] = processTopic(topic)
        
        if(verbose)
            fprintf('Processing topic %s (%d out of %d)\n', pool.Properties.RowNames{ct}, ct, inputParams.topics);
            fprintf('  - run(s): ');
        end;
        
        % reset the index of the run under processing for each topic
        cr = 1;
        
        % compute the measure only on those column which contain the
        % actual runs
        varargout = cellfun(@processRun, topic);
        
        
        % increment the index of the current topic under processing
        ct = ct + 1;
        
        if(verbose)
            fprintf('\n');
        end;
        
        %%
        
        % compute the measure for a given topic of a given run
        function [measure] = processRun(runTopic)
            
            if(verbose)
                fprintf('%s ', runSet.Properties.VariableNames{cr});
            end;
           
             
            % count the number of documents by relevance degree
            if strcmpi(relevanceDegrees{1}, UNSAMPLED_UNJUDGED)
                % poolStats is
                % U_U | NotRelevant | Degree1 | Degree 2
                % so start from NotRelevant (column 2) and take D+1 column
                % (degrees), including column 2
                relevanceCount = poolStats{ct, 2:2+(D)};
            else
                % poolStats is
                % NotRelevant | Degree1 | Degree 2
                % so start from NotRelevant (column 1) and take D+1 column
                % (degrees), including column 1
                relevanceCount = poolStats{ct, 1:1+(D)};
            end;
                     
            % compute the estimator of the Recall Base 
            estimatorRB = sum(relevanceCount.*RelevanceProbability);
            
            % la misura dovrebbe valere 0 se gli utenti hanno soglie di
            % rilevanza alte e la run non recupera documenti che sono
            % considerati rilevanti dagli utenti
             if(estimatorRB == 0)
                 
                 measure = {0};
                 
                 % increment the index of the current run under processing
                 cr = cr + 1;
                 
                 return;
             end;
            

            
            vect = runTopic{:, 'Assessment'};
            % set the relevance probability for each document
            
            for i=1:D+1
                vect(vect==i-1) = RelevanceProbability(i);
            end
            
            % find the positions of the documents relevant with non-zero
            % probability
            relPos = logical(vect);
            
            vect = vect(relPos);
            
            % avoid useless computations when each document is not relevant
            % for the user
              if(length(vect) == 0)
                  
                  measure = {0};
                  
                  % increment the index of the current run under processing
                  cr = cr + 1;
                 
                  return;
              end;
            
            productRT=[1; vect];
            productRT(end)=[];
            
            % compute the mean of random precision at each relevant retrieved document
            p = (vect.*cumsum(productRT)).' ...
                ./ ...
                find(relPos).';
            
            % average over the total number of relevant documents
            measure = sum(p) ./ estimatorRB;
            
            % properly wrap the results into a cell in order to fit it into
            % a value for a table
            measure = {measure};
            
            % increment the index of the current run under processing
            cr = cr + 1;
        end
    end

end



