%% import_collection
%
% Imports the requested collection and saves it to a |.mat| file.
%
%% Synopsis
%
%   [] = import_collection(trackID)
%  
%
% *Parameters*
%
% * *|trackID|* - the identifier of the track to import.
%
% *Returns*
%
% Nothing
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2013b or higher
% * *Copyright:* (C) 2015-2016 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = import_collection(trackID)
    
    % check the number of input arguments
    narginchk(1, 1);

    % set up the common parameters
    common_parameters

    % disable warnings, not needed for bulk import
    warning('off');
    
    % check that trackID is a non-empty string
    validateattributes(trackID,{'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

     if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
    
    % disable warnings, not needed for bulk import
    %warning('off');

    % start of overall import
    startImport = tic;

    fprintf('\n\n######## Importing collection %s (%s) ########\n\n', EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - imported on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - pool %s\n', EXPERIMENT.track.(trackID).pool.file);
    fprintf('  - runs %s\n\n', EXPERIMENT.track.(trackID).run.path);

    % import the pool
    start = tic;

    fprintf('+ Importing the pool\n');
    
    % creating local input parameters for import
    fileName = EXPERIMENT.track.(trackID).pool.file;
    poolID = EXPERIMENT.pattern.identifier.pool(trackID);

    pool = EXPERIMENT.track.(trackID).pool.import(fileName, poolID);
    
    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
    fprintf('  - total number of topics: %d\n\n', height(pool));

    % the list of required topics
    requiredTopics = pool.Properties.RowNames;


    fprintf('+ Importing the run(s)\n');
    
    % creating local input parameters for import
    runID =  EXPERIMENT.pattern.identifier.run(trackID);
    
    runSet = EXPERIMENT.track.(trackID).run.import(EXPERIMENT.track.(trackID).run.path, runID, requiredTopics);
    
    fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
    fprintf('  - total number of run(s): %d\n\n', width(runSet));


    fprintf('+ Saving the data set\n');

    start = tic;
    
    sersave2(EXPERIMENT.pattern.file.dataset(trackID, poolID), ...
                'WorkspaceVarNames', {'pool'}, ...
                'FileVarNames', {poolID});
    
    sersave2(EXPERIMENT.pattern.file.dataset(trackID, runID), ...
                'WorkspaceVarNames', {'runSet'}, ...
                'FileVarNames', {runID});

            
    fprintf('  - elapsed time: %s\n\n', elapsedToHourMinutesSeconds(toc(start)));


    fprintf('\n\n######## Total elapsed time for importing collection %s (%s): %s ########\n\n', ...
        EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startImport)));

    % enable warnings
    warning('on');


end


