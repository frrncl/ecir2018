
common_parameters;

trackID = 'CrowdT21';
trackList = {'T08','T13'};
tracks = length(trackList);
tracks_list = strjoin(trackList, ' '); 

pListID = 'l0';

pListProb = {[0.05 0.95]  [0.10 0.90]};

fprintf('\n\n######## Report the mean correlations (tau and tauAP) and the Root Mean Square Error between the gold pool and the assessed ones for each meausure on %s (%s) ########\n\n', ...
    trackID, EXPERIMENT.label.paper);

fprintf('+ Settings\n');
fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
fprintf('  - tracks %s\n', tracks_list);

Rap005 = EXPERIMENT.pattern.identifier.pid('erap',pListProb{1});
Rap010 = EXPERIMENT.pattern.identifier.pid('erap',pListProb{2});
Rrbp005 = EXPERIMENT.pattern.identifier.pid('errbp',pListProb{1});
Rrbp010 = EXPERIMENT.pattern.identifier.pid('errbp',pListProb{2});

measureLabel = {'nDCG','ERR', 'AP', 'eRAP$_1$', 'eRAP$_2$', 'RBP', 'eRRBP$_1$', 'eRRBP$_2$'};
rowLabel = {'\textbf{Mean } \tau', '\textbf{Mean } \tau_{AP}', '\textbf{Mean RMSE}'};

measures = {'ndcg', 'err', 'ap', Rap005, Rap010, 'rbp', Rrbp005, Rrbp010};

for r = 1:tracks
    
    valuesList = cell(3,length(measures));
    for k = 1:length(measures)
        
        measureID = EXPERIMENT.pattern.identifier.crowdMeasure(measures{k}, EXPERIMENT.label.goldPool, ...
            trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});
        
        tauID = EXPERIMENT.pattern.identifier.pm('tau', measures{k}, EXPERIMENT.label.goldPool, ...
            trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});
        
        apcID = EXPERIMENT.pattern.identifier.pm('apc', measures{k}, EXPERIMENT.label.goldPool, ...
            trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});
        
        rmseID = EXPERIMENT.pattern.identifier.pm('rmse', measures{k}, EXPERIMENT.label.goldPool, ...
            trackID,  EXPERIMENT.track.(trackID).runSet.originalTrackID.shortID{r});
        
        fprintf('+ Loading data\n');
        
        serload2(EXPERIMENT.pattern.file.analysis(trackID, measureID), ...
            'WorkspaceVarNames', {'tauVar','apcVar','rmseVar'}, ...
            'FileVarNames', {tauID, apcID, rmseID});
        
        valuesList{1,k} = tauVar.mean;
        valuesList{2,k} = apcVar.mean;
        valuesList{3,k} = rmseVar.mean;
        
        % free space
        clear('tauVar','apcVar','rmseVar');
        
    end; % correlation
    
    % the file where the report has to be written
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, sprintf('%s_%s', trackID, trackList{r})), 'w');
    
    
    fprintf(fid, '\\documentclass[11pt]{article} \n\n');
    
    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    fprintf(fid, '\\title{Summary Report on the Measure tau and tauAP correlations for collection %s, Trec %s}\n\n', trackID, trackList{r});
    
    fprintf(fid, '\\maketitle\n\n');
    
    %fprintf(fid, '\\begin{landscape}  \n');
    fprintf(fid, '\\begin{table}[p] \n');
    % fprintf(fid, '\\tiny \n');
    fprintf(fid, '\\centering \n');
    %fprintf(fid, '\\hspace*{-6.5em} \n');
    
    fprintf(fid, '\\begin{tabular}{|l|*{%d}{r|}} \n', length(measureID));
    
    fprintf(fid, '\\hline \n');
    
    fprintf(fid, '\\multicolumn{1}{|c|}{ } ');
    
    for m = 1:length(measures)
        fprintf(fid, '& \\multicolumn{1}{c|}{\\textbf{%s}} ', measureLabel{m});
    end; % measure
    
    fprintf(fid, '\\\\ \n');
    
    fprintf(fid, '\\hline \n');
    
    
    % for each correlation pair
    for i = 1:3
        
        % tau
        fprintf(fid, '\\multirow{1}{*}{$%s$} ', rowLabel{i});
        
        for j = 1:length(measures)
            fprintf(fid, '& %.4f ', valuesList{i,j});
        end;
        
        fprintf(fid, '\\\\ \n');
        
        fprintf(fid, '\\hline \n');
    end;
    
    fprintf(fid, '\\end{tabular} \n');
    
    fprintf(fid, '\\caption{Average effects of the $\\tau$ and $\\tau_{AP}$ correlation coefficients and the Root Mean Square Error for each measure computed on the assessed pools with respect the gold one. ');
    fprintf(fid, 'eRAP$_1$ and eRRBP$_1$ have probability values equal to $[%0.02f,%0.02f]$, ', pListProb{1}(1), pListProb{1}(2));
    fprintf(fid, 'eRAP$_2$ and eRRBP$_2$ equal to $[%0.02f,%0.02f]$.} ', pListProb{2}(1), pListProb{2}(2));
    
    fprintf(fid, '\\end{table} \n\n');
     
    fprintf(fid, '\\end{document} \n\n');
    
    fclose(fid);

    diary off;

end


