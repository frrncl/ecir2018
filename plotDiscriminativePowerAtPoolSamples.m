%% plotDiscriminativePowerAtPoolSamples
%
% It plot the discriminative power of a set of runs at differente pool samples 
% for only one iteration.
%
%% Synopsis
%
% plotDiscriminativePowerAtPoolSamples(varargin)
%
% 
% *Name-Value Pair Arguments*
%
% Specify comma-separated pairs of |Name|, |Value| arguments. |Name| is the
% argument name and |Value| is the corresponding value. |Name| must appear
% inside single quotes (' '). You can specify several name and value pair
% arguments in any order as |Name1, Value1, ..., NameN, ValueN|.
%
% * *|Observed|* (mandatory) - a cell vector of strings containing the list
% of measure short names to be considered as observations.
% * *|trackID|* (mandatory) - the identifier of the track to process.
% * *|Reference|* (optional) -  a cell vector of strings containing the list
% of measure short names to be considered as references.
% Reference performances will be plot with a different color and line style
% with respect to observed performances.
% * *|OutputPath|* (optional) - a string specifying the path to the output
% directory where the PDF of the plot will be saved. If not specified, the
% plot will not be saved to a PDF.
% * *|FileNameSuffix|* (optional) - a string to be added in the file name
% for saving the PDF of the plot.
% * *|DpStats|* (optional) -   a boolean indicating whether self
% discriminative powers have to be add to the legend. The default is |false|.
% * *|DeltaStats|* (optional) -   a boolean indicating whether deltas
% have to be add to the legend. The default is |false|.
% * *|PlotSelf|* (optional) -  a cell vector of strings containing the list
% of measure short names for which the self discriminative power plots
% have to be plotted. 

%
%% Example of use
%
%   plotDiscriminativePowerAtPoolSamples('Observed', {'P_10', 'P_100', 'Rprec'}, ...
%                  'Reference', {'AP', 'bpref', 'RBP_080'}, ....
%                  'trackID', 'T13',...
%                  'SelfPlot', 'AP',...            
%                  'DpStats', true, ...
%                  'OutputPath', '/output')
%
% 
% You can note as |Observed| performances are plotted in blu with a
% continuous line while |Reference| performance are plotted in black with a
% dashed line.
%
%% Information
%
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>,
% <mailto:silvello@dei.unipd.it Gianmaria Silvello>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2013b or higher
% * *Copyright:* (C) 2013-2014 <http://ims.dei.unipd.it/ Information
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/
% Department of Information Engineering> (DEI), <http://www.unipd.it/
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License,
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%
function [] = plotDiscriminativePowerAtPoolSamples(varargin)

    common_parameters

    persistent MARKERS ...
        REF_LINESTYLE REF_LINEWIDTH REF_MARKERSIZE ...
        OBS_LINESTYLE OBS_LINEWIDTH OBS_MARKERSIZE ...
        X_LIM_STEP;

    if isempty(MARKERS)

        %MARKERS = {'o', '+', '*', 'x', 's', 'd', '^', 'v', '>', '<', 'p', 'h'};

        MARKERS = {'^', '+', '*', 'v', 'x', 'd', 'o', 's', '>', 'p', 'x', 'h'};
        
        REF_LINESTYLE = '--';
        REF_LINEWIDTH = 1.0;
        REF_MARKERSIZE = 6;

        OBS_LINESTYLE = '-';
        OBS_LINEWIDTH = 1.2;
        OBS_MARKERSIZE = 7;

        X_LIM_STEP = 50;
    end;

    % check that we have the correct number of input arguments.
    narginchk(1, inf);

    % parse the variable inputs
    pnames = {'OutputPath' 'Reference', 'Observed', 'DpStats', 'DeltaStats', 'FileNameSuffix' 'SelfPlot', 'trackID'};
    dflts =  {[]           []           []          false         false            '',             []        ''};
    
    if verLessThan('matlab', '9.2.0')
        [outputPath, reference, observed, dpStats, deltaStats, fileNameSuffix, selfPlot, trackID, supplied] = matlab.internal.table.parseArgs(pnames, dflts, varargin{:});
    else
        [outputPath, reference, observed, dpStats, deltaStats, fileNameSuffix, selfPlot, trackID, supplied] = matlab.internal.datatypes.parseArgs(pnames, dflts, varargin{:});
    end
    

    if supplied.OutputPath
        % check that path is a non-empty string
        validateattributes(outputPath, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'OutputPath');

        if iscell(outputPath)
            % check that path is a cell array of strings with one element
            assert(iscellstr(outputPath) && numel(outputPathpath) == 1, ...
                'MATTERS:IllegalArgument', 'Expected OutputPath to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        outputPath = char(strtrim(outputPath));
        outputPath = outputPath(:).';

        % check if the path is a directory and if it exists
        if ~(isdir(outputPath))
            error('MATTERS:IllegalArgument', 'Expected OutputPath to be a directory.');
        end;

        % check if the given directory path has the correct separator at the
        % end.
        if outputPath(end) ~= filesep;
            outputPath(end + 1) = filesep;
        end;
    end;

    if supplied.FileNameSuffix
        if iscell(fileNameSuffix)
            % check that fileNameSuffix is a cell array of strings with one element
            assert(iscellstr(fileNameSuffix) && numel(fileNameSuffix) == 1, ...
                'MATTERS:IllegalArgument', 'Expected FileNameSuffix to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        fileNameSuffix = char(strtrim(fileNameSuffix));
        fileNameSuffix = fileNameSuffix(:).';

        % check that the nameSuffix is ok according to the matlab rules
        if ~isempty(regexp(fileNameSuffix, '\W*', 'once'))
            error('MATTERS:IllegalArgument', 'FileNameSuffix %s is not valid: it can contain only letters, numbers, and the underscore.', ...
                fileNameSuffix);
        end

        % if it starts with an underscore, remove it since il will be
        % appended afterwards
        if strcmp(fileNameSuffix(1), '_')
            fileNameSuffix = fileNameSuffix(2:end);
        end;

        % if it does not end with and underscore, append it
        if ~strcmp(fileNameSuffix(end), '_')
            fileNameSuffix(end+1) = '_';
        end;
    end;

    if supplied.trackID
        % check that trackID is a non-empty string
        validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

        if iscell(trackID)

            % check that trackID is a cell array of strings with one element
            assert(iscellstr(trackID) && numel(trackID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
        end

        % remove useless white spaces, if any, and ensure it is a char row
        trackID = char(strtrim(trackID));
        trackID = trackID(:).';

        % check that trackID assumes a valid value
        validatestring(trackID, ...
            EXPERIMENT.track.list, '', 'trackID');
    else
        error('MATTERS:MissingArgument', 'Parameter ''Observed'' not provided: the observed performances are mandatory.');
    end;


    if supplied.Observed
        % check that observed is a cell array
        validateattributes(observed, {'cell'}, {'nonempty', 'vector'}, '', 'Observed');

        for k = 1:length(observed)
            % check that each element is a non-empty table
            validateattributes(observed{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Observed{%d}'), 1);
            observed{k} = char(strtrim(observed{k}));
        end;
    else
        error('MATTERS:MissingArgument', 'Parameter ''Observed'' not provided: the observed performances are mandatory.');
    end;

    % markers for the observed performances
    obsMarkers = MARKERS;

    % the labels for the legend, as deduced from the short name of the
    % computed measure
    measures = observed;
    % remove useless white spaces from the title, if any, and ensure it is a char row
    plotTitle = char(strtrim(EXPERIMENT.track.(trackID).name));
    plotTitle = plotTitle(:).';
    plotTitle = strrep(plotTitle, '_', '\_');

    aslData=cell(length(observed),1);
    dpData=cell(length(observed),1);
    deltaData=cell(length(observed),1);
    for k=1:length(observed)

        corrID = EXPERIMENT.pattern.identifier.downsampledDPower(observed{k}, trackID);

        aslID = EXPERIMENT.pattern.identifier.pmMeasure('asl', observed{k}, trackID);
        dpID = EXPERIMENT.pattern.identifier.pmMeasure('dp', observed{k}, trackID);
        deltaID = EXPERIMENT.pattern.identifier.pmMeasure('delta', observed{k}, trackID);

        serload2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
            'WorkspaceVarNames', {'asl','dp','delta'}, ...
            'FileVarNames', {aslID,dpID,deltaID});
        aslData{k} = asl;
        dpData{k} = dp;
        deltaData{k} = delta;

        if (k == 1 && asl.Properties.UserData.iterations ~= 1)
            error('MATTERS:IllegalArgument', 'Expected number of iterations to be equal to 1.');
        end;

        clear('asl','dp','delta');
    end


    if supplied.Reference
        % check that reference is a cell array
        validateattributes(reference, {'cell'}, {'nonempty', 'vector'}, '', 'Reference');

        aslDataRef=cell(length(reference),1);
        dpDataRef=cell(length(reference),1);
        deltaDataRef=cell(length(reference),1);

        for k = 1:length(reference)
            % check that each element is a non-empty table
            validateattributes(reference{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Reference{%d}'), 1);

            corrID = EXPERIMENT.pattern.identifier.downsampledDPower(reference{k}, trackID);

            aslID = EXPERIMENT.pattern.identifier.pmMeasure('asl', reference{k}, trackID);
            dpID = EXPERIMENT.pattern.identifier.pmMeasure('dp', reference{k}, trackID);
            deltaID = EXPERIMENT.pattern.identifier.pmMeasure('delta', reference{k}, trackID);

            serload2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
                'WorkspaceVarNames', {'asl','dp','delta'}, ...
                'FileVarNames', {aslID,dpID,deltaID});
            aslDataRef{k} = asl;
            dpDataRef{k} = dp;
            deltaDataRef{k} = delta;

            clear('asl','dp','delta');

        end;

        % do not use more than half of the markers for the reference
        % performances
        refMarkers = MARKERS(1:min(length(reference), length(MARKERS)/2));

        % assign all the remaining markers for the observed performances
        obsMarkers = MARKERS(length(refMarkers)+1:end);

        % add the short name of the measure as label for the plot
        measures = [measures reference];

    end;
    
    if supplied.DpStats
        % check that selfPlot is a non-empty scalar logical value
        validateattributes(dpStats, {'logical'}, {'nonempty', 'scalar'}, '', 'DpStats');
    end;
    if supplied.DeltaStats
        % check that selfPlot is a non-empty scalar logical value
        validateattributes(deltaStats, {'logical'}, {'nonempty', 'scalar'}, '', 'DeltaStats');
    end;


    samples = length(aslData{1}.Properties.UserData.sampleSize) + 1;
    colors = hsv(samples);

    if supplied.SelfPlot
        % check that observed is a cell array
        validateattributes(selfPlot, {'cell'}, {'nonempty', 'vector'}, '', 'Observed');

        for k = 1:length(selfPlot)
            % check that each element is a non-empty table
            validateattributes(selfPlot{k}, {'char'}, {'nonempty'}, '', num2str(k, 'Observed{%d}'), 1);
            
            corrID = EXPERIMENT.pattern.identifier.downsampledDPower(selfPlot{k}, trackID);

            aslID = EXPERIMENT.pattern.identifier.pmMeasure('asl', selfPlot{k}, trackID);
            dpID = EXPERIMENT.pattern.identifier.pmMeasure('dp', selfPlot{k}, trackID);
            deltaID = EXPERIMENT.pattern.identifier.pmMeasure('delta', selfPlot{k}, trackID);

            serload2(EXPERIMENT.pattern.file.analysis(trackID, corrID), ...
                'WorkspaceVarNames', {'asl','dp','delta'}, ...
                'FileVarNames', {aslID,dpID,deltaID});
            DataAsl{k} = asl;
            DataDp{k} = dp;
            DataDelta{k} = delta;

            clear('asl','dp','delta');

        end;

        % for each measure produce a plot of the ASL of the measure at
        % different pool samples
        for m = 1:length(selfPlot)

            % if output path is supplied, hide the figure to avoid render it on
            % screen when it is actually saved to a file
            if supplied.OutputPath
                h = figure('Visible', 'off');
            else
                h = figure;
            end;
            hold on

            legendLabels = cell(1, samples);

            % plot the observed performances
            for k = 1:samples
                Asl = DataAsl{m}{1, k}{1, 1}{:, :};
                [Asl loc] = sort(Asl(~isnan(Asl)), 1, 'descend');

                plot(Asl, 'Color', colors(k, :), 'LineStyle', OBS_LINESTYLE, ...
                    'Marker', MARKERS{mod(k, length(MARKERS)) + 1}, ...
                    'LineWidth', OBS_LINEWIDTH, 'MarkerSize', OBS_MARKERSIZE, ...
                    'MarkerFaceColor', colors(k, :));


                if k == 1
                    legendLabels{k} = [selfPlot{m} ', original'];
                else
                    switch lower(DataAsl{m}.Properties.UserData.shortDownsampling)
                        case {'srs', 'rs'}
                            legendLabels{k} = sprintf('%s, %d%% sample', selfPlot{m}, ...
                                DataAsl{m}.Properties.UserData.sampleSize(k-1));
                        case 'pds'
                            legendLabels{k} = sprintf('%s, %d% depth', selfPlot{m}, ...
                                DataAsl{m}.Properties.UserData.sampleSize(k-1));
                        otherwise
                            error('MATTERS:IllegalArgument', 'Unrecognized pool downsampling method %s. Only PoolDepthSampling, RandomSampling, and StratifiedRandomSampling are allowed', ...
                                DataAsl{m}.Properties.UserData.downsampling);
                    end;
                end;

                % adjust the legend, if needed
                if dpStats || deltaStats
                    tmpLegendLabels = [legendLabels{k} ' ['];

                    if dpStats
                        tmpLegendLabels = sprintf('%sDP = %4.2f%%', tmpLegendLabels, ...
                            DataDp{m}{1, k}*100);
                    end;

                    if deltaStats
                        if dpStats
                            tmpLegendLabels = [tmpLegendLabels '; '];
                        end;

                        tmpLegendLabels = sprintf('%s\\Delta = %5.2f', tmpLegendLabels, ...
                            DataDelta{m}{1,k});

                    end;

                    legendLabels{k} = [tmpLegendLabels ']'];
                end;

            end

            set(gca, 'FontSize', 14);

            set(gca, 'XLim', [0 (fix(length(Asl)/X_LIM_STEP) + 1)*X_LIM_STEP])

            set(gca, 'YLim', [0 0.15], ...
                'YTick', [0:0.01:0.15], ...
                'YTickLabel', cellstr(num2str([0:0.01:0.15].', '%3.2f')).');

            grid on;

            legendLabels = strrep(legendLabels, '_', '\_');
            hl = legend(legendLabels{:}, 'Location', 'NorthEast');
            set(hl, 'FontSize', 12);

            xlabel('System Pair (sorted by decreasing ASL)')
            ylabel('Achieved Significance Level (ASL)')
            title(plotTitle);

            if ~isempty(outputPath)
                set(h,'PaperPositionMode','manual');
                set(h,'PaperUnits','normalized');
                set(h,'PaperPosition',[0.05 0.05 0.9 0.9]);
                set(h,'PaperType','A4');
                set(h,'PaperOrientation','landscape');
                print(h, '-dpdf', sprintf('%1$ssDiscriminativePowerAtPoolSamples_%2$s_%3$s.pdf', outputPath, measures{m},trackID));
                close(h);
            end;

        end;
    clear('DataAsl','DataDp','DataDelta')
    end;




    %Da sistemare questo una volta caricate le statistiche delle misure
    switch lower(aslData{1}.Properties.UserData.shortMethod)
        case {'pbt'}
            plotTitle = {plotTitle; 'ASL using Paired Bootstrap Test'};
        case 'rthsdt'
            plotTitle = {plotTitle; 'ASL using Randomised Tukey HSD Test'};
        otherwise
            error('MATTERS:IllegalArgument', 'Unrecognized discriminative power method %s. Only Paired Bootstrap Test and Randomised Tukey HSD Test are allowed', ...
                aslData{1}.Properties.UserData.method);
    end;

    measures = [observed reference];
    obsColors = winter(length(observed));
    refColors = bone(length(reference)+2);
    origPlotTitle = plotTitle{2};

    for k = 1:samples


        if k == 1
            plotTitle{2} = [origPlotTitle ', Original Pool'];
            
            if ~isempty(outputPath)
                sfx = sprintf('Original');
            end
            
        else
            switch lower(aslData{1}.Properties.UserData.shortDownsampling)
                case {'srs', 'rs'}
                    plotTitle{2} = sprintf('%s, %d%% Pool Reduction Rate', origPlotTitle, ...
                        aslData{1}.Properties.UserData.sampleSize(k-1));
                    if ~isempty(outputPath)
                        sfx = sprintf('PoolDepth_%d', aslData{1}.Properties.UserData.sampleSize(k-1));
                    end
                case 'pds'
                    plotTitle{2} = sprintf('%s, %d% Pool Depth', origPlotTitle, ...
                        aslData{1}.Properties.UserData.sampleSize(k-1));
                    if ~isempty(outputPath)
                        sfx = sprintf('PoolDepth_%d', aslData{1}.Properties.UserData.sampleSize(k-1));
                    end
                otherwise
                    error('MATTERS:IllegalArgument', 'Unrecognized pool downsampling method %s. Only PoolDepthSampling, RandomSampling, and StratifiedRandomSampling are allowed', ...
                        aslData{1}.Properties.UserData.downsampling);
            end;
        end;

        legendLabels = measures;

        legendLabels = strrep(legendLabels, '_', '\_');
        
        
        % if output path is supplied, hide the figure to avoid render it on
        % screen when it is actually saved to a file
        if supplied.OutputPath
            h = figure('Visible', 'off');
        else
            h = figure;
        end;
        hold on
        
        % plot the observed performances
        for m = 1:length(observed)
            
            Asl = aslData{m}{1, k}{1, 1}{:, :};
            [Asl loc] = sort(Asl(~isnan(Asl)), 1, 'descend');
            
            plot(Asl, 'Color', obsColors(m, :), 'LineStyle', OBS_LINESTYLE, ...
                'Marker', obsMarkers{mod(m, length(obsMarkers)) + 1}, ...
                'LineWidth', OBS_LINEWIDTH, 'MarkerSize', OBS_MARKERSIZE, ...
                'MarkerFaceColor', obsColors(m, :));
            
            % adjust the legend, if needed
            if dpStats || deltaStats
                
                legendLabels{m} = [legendLabels{m} ' ['];
                
                if dpStats
                    legendLabels{m} = sprintf('%sDP = %4.2f%%', legendLabels{m}, ...
                        dpData{m}{1, k}*100);
                end;
                
                if deltaStats
                    if dpStats
                        legendLabels{m} = [legendLabels{m} '; '];
                    end;
                    
                    legendLabels{m} = sprintf('%s\\Delta = %4.2f', legendLabels{m}, ...
                        deltaData{m}{1, k});
                end;
                legendLabels{m} = [legendLabels{m} ']'];
                
            end;
        end
        
        % plot the reference performances
        for m = 1:length(reference)
            
            Asl = aslDataRef{m}{1, k}{1, 1}{:, :};
            [Asl loc] = sort(Asl(~isnan(Asl)), 1, 'descend');
            
            plot(Asl, 'Color', refColors(m, :), 'LineStyle', REF_LINESTYLE, ...
                'Marker', refMarkers{mod(m, length(refMarkers)) + 1}, ...
                'LineWidth', REF_LINEWIDTH, 'MarkerSize', REF_MARKERSIZE, ...
                'MarkerFaceColor', refColors(m, :));
            
            % adjust the legend, if needed
            if dpStats || deltaStats
                
                legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ' ['];
                
                if dpStats
                    legendLabels{m+length(observed)} = sprintf('%sDP = %4.2f%%', legendLabels{m+length(observed)}, ...
                        dpDataRef{m}{1, k}*100);
                end;
                
                if deltaStats
                    if dpStats
                        legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} '; '];
                    end;
                    
                    legendLabels{m+length(observed)} = sprintf('%s\\Delta = %4.2f', legendLabels{m+length(observed)}, ...
                        deltaDataRef{m}{1, k});
                end;
                
                legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ']'];
            end;
            
        end;
        
        set(gca, 'FontSize', 14);
        
        set(gca, 'XLim', [0 (fix(length(Asl)/X_LIM_STEP) + 1)*X_LIM_STEP])
        
        set(gca, 'YLim', [0 0.15], ...
            'YTick', [0:0.01:0.15], ...
            'YTickLabel', cellstr(num2str([0:0.01:0.15].', '%3.2f')).');
        
        grid on;
        
        hl = legend(legendLabels{:}, 'Location', 'NorthEast');
        set(hl, 'FontSize', 12);
        
        xlabel('System Pair (sorted by decreasing ASL)')
        ylabel('Achieved Significance Level (ASL)')
        title(plotTitle);
        
        if ~isempty(outputPath)
            set(h,'PaperPositionMode','manual');
            set(h,'PaperUnits','normalized');
            set(h,'PaperPosition',[0.05 0.05 0.9 0.9]);
            set(h,'PaperType','A4');
            set(h,'PaperOrientation','landscape');
            print(h, '-dpdf', sprintf('%1$sxDiscriminativePower_AtPoolSamples_%2$s%3$s_%4$s.pdf', outputPath, fileNameSuffix, sfx, trackID));
            close(h);
        end;
    end
    
    
    
    % if output path is supplied, hide the figure to avoid render it on
    % screen when it is actually saved to a file
    if supplied.OutputPath
        h = figure('Visible', 'off');
    else
        h = figure;
    end;
    hold on
    
    legendLabels = measures;

    legendLabels = strrep(legendLabels, '_', '\_');
    
    % plot the observed performances
    for m = 1:length(observed)
        
        dp = dpData{m}{:, :};
                
        plot(1:length(dp), dp, 'Color', obsColors(m, :), 'LineStyle', OBS_LINESTYLE, ...
            'Marker', obsMarkers{mod(m, length(obsMarkers)) + 1}, ...
            'LineWidth', 3, 'MarkerSize', OBS_MARKERSIZE, ...
            'MarkerFaceColor', obsColors(m, :));        
        
        
        % adjust the legend, if needed
%         if dpStats
%             legendLabels{m} = [legendLabels{m} ' ['];
%             
%             legendLabels{m} = sprintf('%sDP = %4.2f%%', legendLabels{m}, ...
%                 dp(1)*100);
%             
%             legendLabels{m} = [legendLabels{m} ']'];
%             
%         end               
    end
    
    % plot the reference performances
    for m = 1:length(reference)
        
        dp = dpDataRef{m}{:, :};
        
        plot(1:length(dp), dp, 'Color', refColors(m, :), 'LineStyle', REF_LINESTYLE, ...
            'Marker', refMarkers{mod(m, length(refMarkers)) + 1}, ...
            'LineWidth', 2, 'MarkerSize', REF_MARKERSIZE, ...
            'MarkerFaceColor', refColors(m, :));
        
        % adjust the legend, if needed
%         if dpStats
%             legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ' ['];
%             
%             legendLabels{m+length(observed)} = sprintf('%sDP = %4.2f%%', legendLabels{m+length(observed)}, ...
%                 dp(1)*100);
%             
%             legendLabels{m+length(observed)} = [legendLabels{m+length(observed)} ']'];
%             
%         end                
    end
    
    switch lower(dpData{1}.Properties.UserData.shortDownsampling)
        case 'srs'
            xTickLabels = [100 dpData{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Discriminative Power with Stratified Random Pool Sampling  '};
        case 'rs'
            xTickLabels = [100 dpData{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Discriminative Power with Random Pool Sampling  '};
        case 'pds'
            xTickLabels = [100 dpData{1}.Properties.UserData.sampleSize];
            xTickLabels = strtrim(cellstr(num2str(xTickLabels.', '%d%%')));
            xLabel = 'Pool Reduction Rate';
            plotTitle = {plotTitle,  'Discriminative Power with Pool Depth Sampling  '};
        otherwise
            error('MATTERS:IllegalArgument', 'Unrecognized pool downsampling method %s. Only PoolDepthSampling, RandomSampling, and StratifiedRandomSampling are allowed', ...
                dpData{1}.Properties.UserData.downsampling);
    end
    
        
    set(gca, 'FontSize', 18);
    set(gca,'XTick', 1:length(xTickLabels));
    set(gca,'XTickLabel', xTickLabels);
    
    set(gca, 'YLim', [0 1], ...
        'YTick', 0:0.1:1, ...
        'YTickLabel', strtrim(cellstr(num2str([0:10:100].', '%d%%'))));
    
    grid on;
    
    hl = legend(legendLabels{:}, 'Location', 'NorthEast');
    set(hl, 'FontSize', 12);
    
    xlabel(xLabel)
    ylabel('Discriminative Power')
%    title(plotTitle);
    
    if ~isempty(outputPath)
        set(h,'PaperPositionMode','manual');
        set(h,'PaperUnits','normalized');
        set(h,'PaperPosition',[0.05 0.05 0.9 0.9]);
        set(h,'PaperType','A3');
        set(h,'PaperOrientation','landscape');
        print(h, '-dpdf', sprintf('%sDP_AtPoolSamples_%s_%s.pdf', outputPath, fileNameSuffix, trackID));
        close(h);
    end;
    
    

end



